<?php
namespace App;

use Eloquent;

class TransportMarkup extends Eloquent {

	protected $table = 'ztransportmarkups';

    protected $guarded = array('id');
    protected $softDelete = true;
    protected $dates = ['deleted_at'];       

    public static function getTransportMarkupList ($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {
        return TransportMarkup::from('ztransportmarkups as tm')
                ->leftJoin('ztransportmarkuppercentages as tmp','tmp.id','=','tm.transport_markup_percentage_id')
                ->leftJoin('ztransportmarkupagentcommissions as tmac','tmac.id','=','tm.transport_markup_agent_commission_id')
                ->leftJoin('zhotelmarkupsuppliercommissions as tmsc','tmsc.id','=','tm.transport_markup_supplier_commission_id')
                ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                    if($sSearchBy == 'tm.name'){
                        $query->where('tm.name','like','%'.$sSearchStr.'%');
                    }
                    elseif($sSearchBy == 'tm.allocation_type'){
                        $query->where('tm.allocation_type','like','%'.$sSearchStr.'%');
                    }
                    elseif($sSearchBy == 'tmp.percentage'){
                        $query->where('tmp.percentage','like','%'.$sSearchStr.'%');
                    }
                    elseif($sSearchBy == 'tmac.percentage'){
                        $query->where('tmac.percentage','like','%'.$sSearchStr.'%');
                    }
                })
            ->orderBy($sOrderField, $sOrderBy)
            ->select(
                        'tm.id as id',
                        'tm.is_default as is_default',
                        'tm.is_active as is_active',
                        'tm.name as name',
                        'tm.allocation_type as allocation_type',
                        'tm.allocation_id as allocation_id',
                        'tmp.percentage as markup_percentage',
                        'tmac.percentage as agent_commition'
                    )
            ->paginate($nShowRecord);
    }


    public function markedup_price(){
    	return $this->belongsTo('TransportMarkedupPrice','transport_markup_id','id');
    }    

    public function markup_percentage(){ 
        return $this->hasOne('\App\TransportMarkupPercentage','transport_markup_id','id');
    }        

    public function agent_commission(){
        return $this->hasOne('\App\TransportMarkupAgentCommission','transport_markup_id','id');
    }           

    public function supplier_commission(){ 
        return $this->hasOne('\App\TransportMarkupSupplierCommission','transport_markup_id','id');
    }         

    public function scopeGet_transport($query){
        $query->join('zTransport', function($join)
            {
                $join->on('zTransportMarkups.allocation_id', '=', 'zTransport.id')
                     ->where('zTransportMarkups.allocation_type', '=', 'transport');
            })
            ->select('zTransportMarkups.id', 'zTransportMarkups.name', 'zTransportMarkups.allocation_type', 'zTransportMarkups.is_active', 'zTransport.name AS allocation_name', 'zTransportMarkups.created_at');
    }

    public function scopeGet_city($query){
        $query->join('zCities', function($join)
            {
                $join->on('zTransportMarkups.allocation_id', '=', 'zCities.id')
                     ->where('zTransportMarkups.allocation_type', '=', 'city');
            })
            ->select('zTransportMarkups.id', 'zTransportMarkups.name', 'zTransportMarkups.allocation_type', 'zTransportMarkups.is_active', 'zCities.name AS allocation_name', 'zTransportMarkups.created_at');
    }    

    public function scopeGet_country($query){
        $query->join('zCountries', function($join)
            {
                $join->on('zTransportMarkups.allocation_id', '=', 'zCountries.id')
                     ->where('zTransportMarkups.allocation_type', '=', 'country');
            })
            ->select('zTransportMarkups.id', 'zTransportMarkups.name', 'zTransportMarkups.allocation_type', 'zTransportMarkups.is_active', 'zCountries.name AS allocation_name', 'zTransportMarkups.created_at');
    }    

    public function scopeActive($query){
        $query->where(['is_active' => 1]);
    }

    public function scopeDefault($query){
        $query->where(['is_default' => 1]);
    }
}
 
?>