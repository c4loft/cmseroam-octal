<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelRoomType extends Model
{
    protected $table = 'zhotelroomtypes';
    protected $guarded = [];
    protected $primaryKey = 'id';
    
    public static function getRoomType($sSearchStr,$sOrderField,$sOrderBy)
    {
        return HotelRoomType::from('zhotelroomtypes')
                            ->when($sSearchStr, function($query) use($sSearchStr) {
                                    $query->where('name','like','%'.$sSearchStr.'%');
                                })
                            ->orderBy($sOrderField, $sOrderBy)
                            ->select('*')
                            ->get();
    }
}
