<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'first_name', 'last_name', 'user_id','email','search_history','social_id','currency','is_confirmed','contact_no','pref_contact_method','freq_used_products','pref_hotel_categories','pref_hotel_room_types','pref_transport_types','pref_cabin_class','pref_seat_type','pref_meal_type','interests','additional_info'
    ];
    protected $table = 'zcustomers';
    protected $primaryKey = 'id';

    public function updateDetails($customer) {
    	$preferredHotelCategories = HotelCategory::whereNameIn($customer['preference']['hotel_categories'])
    		->get(['id'])
    		->toArray();

    	$preferredHotelRoomType = HotelRoomType::whereNameIn($customer['preference']['hotel_room_types'])
    		->get(['id'])
    		->toArray();

    	$preferredAgeGroupId = AgeGroup::whereNameIn($customer['preference']['age_groups'])
    		->get(['id'])
    		->toArray();

    	$preferredNationality = Nationality::whereNameIn($customer['preference']['nationality'])
    		->get(['id'])
    		->toArray();

    	$preferredGender = $customer['preference']['gender'];

    	$preferredTransportTypes = TransportType::whereNameIn($customer['preference']['transport_types'])
    		->get(['id'])
    		->toArray();

    	$preferredCabinClass = $customer['preference']['cabin_class'];

    	$preferredSeatType = $customer['preference']['seat_types'];

    	$preferredMealType = $customer['preference']['meal_types'];

    	$preferredActivities = ActivityInterest::whereNameIn($customer['preference']['activities'])
    		->get(['id'])
    		->toArray();

    	$this->first_name 				= $customer['first_name'];
		$this->last_name 				= $customer['last_name'];
		$this->dob 						= $customer['date_of_birth'];
		$this->is_lead 					= $customer['is_lead_traveller'];
		$this->customer_id 				= $customer['customer_id'];
		$this->phy_address_1 			= $customer['address']['address_1'];
		$this->phy_address_2 			= $customer['address']['address_2'];
		$this->phy_city 				= $customer['address']['city'];
		$this->phy_state 				= $customer['address']['state'];
		$this->phy_country 				= $customer['address']['country'];
		$this->phy_zip 					= $customer['address']['zip'];
		$this->bill_address_1 			= $customer['billing_address']['address_1'];
		$this->bill_address_2 			= $customer['billing_address']['address_2'];
		$this->bill_city 				= $customer['billing_address']['city'];
		$this->bill_state 				= $customer['billing_address']['state'];
		$this->bill_country 			= $customer['billing_address']['country'];
		$this->bill_zip 				= $customer['billing_address']['zip'];
		$this->pref_hotel_categories 	= implode(',', $preferredHotelCategories);
		$this->pref_hotel_room_types 	= implode(',', $preferredHotelRoomType);
		$this->pref_age_group_id		= implode(',', $preferredAgeGroupId);
		$this->pref_nationality_id		= implode(',', $preferredNationality);
		$this->pref_gender 				= $preferredGender;
		$this->pref_transport_types 	= implode(',', $preferredTransportTypes);
		$this->pref_cabin_class 		= implode(',', $preferredCabinClass);
		$this->pref_seat_type 			= implode(',', $preferredSeatType);
		$this->pref_meal_type 			= implode(',', $preferredMealType);
		$this->interests 				= implode(',', $preferredActivities);
		$this->save();
    }
}
