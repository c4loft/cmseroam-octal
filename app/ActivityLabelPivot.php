<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityLabelPivot extends Model
{
    protected $table = 'zactivitylabelspivot';
    protected $fillable = ['activity_id','label_id'];
    protected $primaryKey = 'id';
    public $timestamps = false; 
    public function Label(){
        return $this->hasOne('App\Label','id','label_id');
    }
}
