<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityPropertyList extends Model
{
    protected $connection= 'mariadb';

    protected $table = 'activepropertylist';

    public static function geActivityPropertyList($nCountryId,$sCity,$sOrderField,$sOrderBy,$nShowRecord = 10){
        return ActivityPropertyList::from('activepropertylist as ap')
				                    ->where('Country',$nCountryId)
				                    ->when($sCity, function($query) use($sCity) {
				                            $query->where('City',$sCity);
				                        })
				                    ->select('*')
				                    ->orderBy($sOrderField, $sOrderBy)
				                    ->paginate($nShowRecord);
    }
}
