<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderPickup extends Model
{
    protected $fillable = [
        'provider_id','pickup_time','description'
    ];
    protected $table = 'tblproviderpickups';
    protected $primaryKey = 'pickup_id';
    public $timestamps = false;
}
