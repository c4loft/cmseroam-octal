<?php
namespace App;
use Eloquent;
class TransportType extends Eloquent {
    protected $table = 'ztransporttypes';
    protected $guarded = [];
    public $timestamps = false;
    protected $primaryKey = 'id';
    
    public function transport(){
    	 return $this->hasMany('\App\Transport');
    }
    
    public static function getTransportTypeList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {
        return Transport::from('ztransporttypes as t')
                        ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                                    $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                                })
                        ->select('t.*')     
                        ->orderBy($sOrderField, $sOrderBy)
                        ->paginate($nShowRecord);  
    }

  
    
}