<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HBDestination extends Model
{
    protected $table = 'zhbdestinations';
    protected $primaryKey = 'id';
    protected $fillable = [
        'destination_name','destination_code','country_code'
    ];
    
    public function hbZones()
    {
        return $this->hasMany('App\HBZone', 'hb_destination_id', 'id');
    }
    
    public static function getHbDestination($sCountryCode) 
    {
    return HBDestination::select('id', 'destination_code', 'destination_name', 'country_code')
                        ->where('country_code', $sCountryCode)
                        ->orderBy('destination_name')
                        ->get();    
    }
}
