<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaggingMystifly extends Model
{
    protected $table = 'stagging_mystifly';
    protected $guarded = [];
    protected $primaryKey = 'id';
    protected $casts = [
     'fare_rules' => 'array'
	];
}
