<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class ActivityOperator extends Model
{
    protected $fillable = [
        'name','logo','description','special_notes','remarks','marketing_contact_name','marketing_contact_email','marketing_contact_title',
        'marketing_contact_phone','reservation_contact_name', 'reservation_contact_email', 'reservation_contact_landline','reservation_contact_free_phone',
        'accounts_contact_name','accounts_contact_email','accounts_contact_title','accounts_contact_phone',
    ];
    protected $table = 'zactivityoperators';
    protected $primaryKey = 'id';
    
    public static function getActivityOperatorList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {
        return ActivityOperator::from('zactivityoperators as ao')                                                        
                                ->when($sSearchStr, function($query) use($sSearchStr) {
                                        $query->where('name','like','%'.$sSearchStr.'%');
                                    })
                                ->select('*')
                                ->orderBy($sOrderField, $sOrderBy)
                                ->paginate($nShowRecord);
        
    }
}
