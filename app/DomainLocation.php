<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomainLocation extends Model{
    protected $table = 'domain_locations';
    protected $fillable = ['city_id','region_id','domain_id','licensee_id','country_id','created_by'];

    public function location_country() {
        return $this->belongsTo('App\Country','country_id');
    }

    public function location_city() {
        return $this->belongsTo('App\City','city_id');
    }

    public function location_country_with_region() {
        return $this->belongsTo('App\Country','country_id')->with('region');
    }
}
