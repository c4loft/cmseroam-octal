<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $permissions = config('permission');     
        foreach($permissions as $type => $rolePermissions) {
            foreach($rolePermissions as $rolePermission) {
                Gate::define($rolePermission, function ($user) use($permissions, $rolePermission) {
                    return in_array($rolePermission, $permissions[$user->type]);
                });
            }
        }
    }
}
