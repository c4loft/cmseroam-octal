@extends( 'layout/mainlayout' )
@section('custom-css')
    <style>
        .success_message{
            color:green !important;
            text-align: center;
        }
        .input-group-btn .btn .btn-default{ padding: 10px; }
    </style>
@stop
@section('content')
	<div class="content-container">
    <h1 class="page-title">Manage Hotel Mapping</h1>
    <?php 
                                    
    $city_id    = '';
    $country_id = $country;
    $hotelIDs   = '';
    foreach ($hotel_ids as $hotelID) {
        $hotelIDs = $hotelID->EANHotelID.",".$hotelIDs;
    }
    $hotelIDs   = explode(",",$hotelIDs);
    
    ?>
    	@if (Session::has('message'))
           <div class="small-6 small-centered columns success_message">{{ Session::get('message') }}</div>
           <br>
        @endif
        <div class="box-wrapper">
        	<div class="row m-t-20 search-wrapper">
	        <div class="col-md-7 col-sm-7">
	        	{{Form::open(array('url' => 'acomodation/hotel-mapping','method'=>'Get')) }}
	        	<?php $attributes = 'form-control' ?>
		        <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">Select Country <span class="required">*</span></label>
                            <?php $countries =  $countries; ?>
                            {{Form::select('country_id',$countries,$country,['id'=>'country_id','class'=>'form-control select-country'])}}
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                        	<label class="label-control">Select City <span class="required">*</span></label>
                            <select id="city" class="form-control select-city" name="city" disabled="disabled">
                                <option value="">Select City</option>
                                <?php 
                                    if($cities) {
                                        
                                        foreach ($cities as $key => $value) {

                                            $selected = '';
                                            if($city == $key)
                                            {
                                                $selected   = "selected";
                                                $city_id    = $key;
                                            }

                                            echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-1">
	                    <span class="input-group-btn">
		                 	<button class="btn btn-default" type="submit"><i class="icon-search-domain"></i></button>
		                </span>
		            </div>
                </div>

                {{Form::close()}}
	        </div>
	       
		        <input type="hidden" id="country_id" value="{{$country_id}}">
                <input type="hidden" id="city_id" value="{{$city_id}}">
                <div class="col-md-5 col-sm-5">
                  	<div class="dropdown" onClick="this.disabled=true">
                    	<button onClick="this.disabled=true" class="btn btn-primary btn-block dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >ACTION</button>
                    	<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                      		<li><a href="javascript:void(0)" id="map_hotels">Map Hotels</a></li>
                    	</ul>
                  	</div>
                </div>
            </div> 
            <input type="hidden" name="order_field" value="{{ $sOrderField }}" />
            <input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
            <div class="table-responsive m-t-20 table_record">
                @include('WebView::hotel_mapping._more_hotel_mapping')
            </div>
            <div>
                @if(isset($propertyList) && count($propertyList) > 0)
                <div class="pagination-margin">
                  <ul class="pagination">
  
                    
                  </ul>
                </div>
                @endif
            </div>
        </div>
        
	  
</div>
@stop
@section('custom-js')
<script>
@if(isset($propertyList) && count($propertyList) > 0)
    $(function() {
        $('.pagination').pagination({
            pages: {{ $propertyList->lastPage() }},
            itemsOnPage: 10,
            currentPage: {{ $propertyList->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
                var countryid = $('#country_id').val();
                var cityid = $('#city_id').val();
                getPaginationListing(siteUrl('acomodation/hotel-mapping?country_id='+countryid+'&city='+cityid+'&page='+pageNumber),event,'table_record');

                $('#checkbox-00').prop('checked',false);
                setupLabel();
            }
        });
    });
@endif
function getSortData(element,sOrderField)
{
    if($(element).find( "i" ).hasClass('fa-caret-down'))
    {
        $(element).find( "i" ).removeClass('fa-caret-down');
        $(element).find( "i" ).addClass('fa-caret-up');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('desc');
    }
    else
    {
        $(element).find( "i" ).removeClass('fa-caret-up');
        $(element).find( "i" ).addClass('fa-caret-down');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('asc');
    }
    var countryid = $('#country_id').val();
    var cityid = $('#city_id').val();
    getMoreListing(siteUrl('acomodation/hotel-mapping?country_id='+countryid+'&city='+cityid+'&page='+$('#page_no').val()),event,'table_record');
}
    $( '.select-country' ).change( function() {
        getCity();
    });
    
    function getCity(){
        var value = $( '.select-country' ).val();
        var city = $( '#city' ).val();
        showLoader()
        //console.log(city.id);
        if(value != ''){
            $.ajax({
                url: "{{ route('common.get-cities-by-country') }}",
                method: 'post',
                data: {
                    country_id: value,
                    _token: '{{ csrf_token() }}'
                },
                success: function( response ) 
                {
                    var selectCity = $('.select-city');
                    selectCity.html('<option value="" selected>Select City</option>');
                    for(var i = 0; i < response.length; i++) {
                        var selected = '';
                        /*if(city = response[i].id) 
                            selected = "selected";*/
                        var html = '<option value="'+ response[i].id +'" '+selected+'>'+ response[i].name +'</option>';
                        $( '.select-city' ).append(html);
                        $('.select-city').prop('disabled',false);
                    }
                    hideLoader();
                }
            });
        }
    }


    $(document).on('click','.select_all_chk',function(){
        if (this.checked) 
        {
            $(".chkbox").find('input[type=checkbox]').each(function () {
                    this.checked = true;                        
            });
        }
        else
        {
            $(".chkbox").find('input[type=checkbox]').each(function () {
                    this.checked = false;                       
            });
        }
        setupLabel();
    });

    $(document).on('click','.chkboxes',function(){
        if($('.chkboxes:checked').length == $('.chkboxes').length){
            $('#checkbox-00').prop('checked',true);
        }else{
            $('#checkbox-00').prop('checked',false);
        }
        setupLabel();
    });

    $("#map_hotels").click(function(){
        var country_id  = $("#country_id").val();
        var city_id     = $("#city_id").val();
        var hotel_id    = $(this).data('hotelId');
        var hotelId    = [];
        $('.chkboxes:checked').each(function(){
            var id = $(this).val();            
            hotelId.push(id);
        });
        $.ajax({
            url: 'hotel/mapping/add',
            method: 'post',
            data: {
                country_id: country_id,
                city_id : city_id,
                hotel_id: hotelId,
                _token: '{{ csrf_token() }}'
            },
            success: function( response ) {
                if ( response != null ) {
                    window.location.reload();
                }
            }
        });
    });
</script>
@stop