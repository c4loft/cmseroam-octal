@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">

    @if(isset($oRoom))
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Accomodation Room Type']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Accomodation Room Type']) }}</h1>
    @endif
    <div class="row">
        @if (Session::has('message'))
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif

    </div>
    <br>
    @if(isset($oRoom))
    {{ Form::model($oRoom, array('url' => route('acomodation.hotel-roomtype-create') ,'method'=>'POST','enctype'=>'multipart/form-data')) }}
    @else
    {{Form::open(array('url' => 'acomodation/hotel-roomtype-create','method'=>'Post','enctype'=>'multipart/form-data')) }}
    @endif
        <div class="box-wrapper">
            <div class="form-group m-t-30">
                <label class="label-control">Room Type <span class="required">*</span></label>
                {{Form::text('name',Input::old('name'),['id'=>'name','class'=>'form-control','placeholder'=>'Enter Room Type'])}}
            </div>

            @if ( $errors->first( 'name' ) )
            <small class="error">{{ $errors->first('name') }}</small>
            @endif
            
            <div class="form-group m-t-30">
                <label class="label-control">Pax </label>
                {{ Form::text('pax',Input::old('pax'),['id'=>'pax','class'=>'form-control','placeholder'=>'Enter Pax']) }}
            </div>  
        </div>
    
        <div class="m-t-20 row col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-sm-6">
                     <input type="hidden" value="{{ $nIdRoom }}" name="id" >
                {{Form::submit('Save',['class'=>'button success btn btn-primary btn-block']) }}
                </div>
                <div class="col-sm-6">
                  <a href="{{ route('acomodation.hotel-room-list')}}" class="btn btn-primary btn-block">Cancel</a>
                </div>
            </div>
        </div>        
    {{Form::close()}}
</div>
@stop

@section('custom-css')
<style>    
    .error{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }
    
    div .with_error{
        border:1px solid black;
    }
    .error_message{
        color:red !important;
    }
    
    .success_message{
        color:green !important;
        text-align: center;
    }
</style>
@stop

@section('custom-js')
<script>
    tinymce.init({
        selector:'#description',
        height: 200,
        menubar: false
    });    
</script>
@stop
