@foreach ($oHotelList as $aHotel)	
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aHotel->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aHotel->id;?>" value="<?php echo $aHotel->id;?>">&nbsp;
            </label>
        </td>
        <td>{{ $aHotel->SupplierCode }}</td>
        <td>{{ $aHotel->Name }}</td>
        <td>{{ $aHotel->LocationID }}</td>
        <td> {{ $aHotel->LocationName }} </td>
        <td>{{ $aHotel->Address1 }}</td>
        <td>
            <a href="{{ route('acomodation.aotsupplier-update',[ 'nIdSupplier' => $aHotel->id ])}}" class="button success tiny btn-primary btn-sm">{{ trans('messages.update_btn') }}</a>
        </td>
    </tr> 
@endforeach