<table class="table">
    <thead>
        <tr>
			<th onclick="getTourSort(this,'t.tour_code');">Tour Code <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 't.tour_code')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getTourSort(this,'FName');">Customer Name <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'FName')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
			<th onclick="getTourSort(this,'request_date');">Created Date <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'request_date')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
			<th onclick="getTourSort(this,'departure_date');">Departure Date <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'departure_date')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
			<th onclick="getTourSort(this,'return_date');">Return Date 
                 <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'return_date')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
			<th onclick="getTourSort(this,'Agent_id');">Agent (Consultant)
                 <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'Agent_id')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
			<th class="text-center">{{ trans('messages.thead_action') }}</th>
        </tr>
    </thead>
    <tbody class="tour_list_ajax">
    @if(count($oBookingRequest) > 0)
        @include('WebView::booking._more_tour_status_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oBookingRequest->count() , 'total'=>$oBookingRequest->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
        
      </ul>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.pagination').pagination({
            pages: {{ $oBookingRequest->lastPage() }},
            itemsOnPage: 10,
            currentPage: {{ $oBookingRequest->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
				getBookingPaginationListing(siteUrl('booking/tour-booking-listing?page='+pageNumber),event,'table_record');
                //getPaginationListing(siteUrl('tour/tour-list?page='+pageNumber),event,'table_record');
                /*if(pageNumber > 1)
                    callTourListing(event,'tour_list_ajax',pageNumber);
                else
                    callTourListing(event,'table_record',pageNumber);*/
                $('#checkbox-00').prop('checked',false);
                setupLabel();
            }
        });
    });

    var cmp_tour = [] ; 
    $(document).on('click',".cmp_tour_check",function () {
        if(this.checked) {
            if(jQuery.inArray($(this).val(), cmp_tour) == -1)
                cmp_tour.push($(this).val());
            $('#dropdownMenu1').prop('disabled', false);
        }else{
            var removeItem = $(this).val();
            cmp_tour = $.grep(cmp_tour, function(value) {
                            return value != removeItem;
                          });
        }
        
        if(cmp_tour.length > 1){
            $("#manage_views, #manage_dates").hide();
        }
        else
            $("#manage_views, #manage_dates").show();
        
        if(cmp_tour.length == 0){
            $('#dropdownMenu1').prop('disabled', true);
        }
});

$(document).on('click','.cmp_tour_check',function(){
    if($('.cmp_tour_check:checked').length == $('.cmp_tour_check').length){
        $('#checkbox-00').prop('checked',true);
    }else{
        $('#checkbox-00').prop('checked',false);
    }
});

$(document).on('click','.label_check',function(){
    setupLabel();
});
</script>