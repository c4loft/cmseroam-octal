@extends( 'layout/mainlayout' )
@section( 'content' )
<div class="content-container">
    <h1 class="hide"></h1>
    <section>
        <h2 class="page-title">eRoam Dashboard</h2>
        <div class="row">
            <div class="col-sm-4">
                <div class="dashboard-box-wrapper">
                    <div class="dashboard-box-inner">
                        <div class="dashboard-box">
                            <div class="row">
                                <div class="col-sm-10">
                                    <p>Supplier Bookings</p>
                                </div>
                                <div class="col-sm-2 text-right"><span class="border-round"></span><span class="border-round fill"></span></div>
                            </div>
                            <div class="table-responsive m-t-20">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>Hotel - To Contact</td>
                                            <td>144</td>
                                            <td><a href="#"><i class="fa fa-question-circle-o"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Hotel - Contacted</td>
                                            <td>22</td>
                                            <td><a href="#"><i class="fa fa-question-circle-o"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Transport - To Contact</td>
                                            <td>155</td>
                                            <td><a href="#"><i class="fa fa-question-circle-o"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Transport - Contacted</td>
                                            <td>02</td>
                                            <td><a href="#"><i class="fa fa-question-circle-o"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Activity - To Contact</td>
                                            <td>234</td>
                                            <td><a href="#"><i class="fa fa-question-circle-o"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Activity - Contacted</td>
                                            <td>02</td>
                                            <td><a href="#"><i class="fa fa-question-circle-o"></i></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box-border">
                        <div class="dropdown more-link">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Last 30 Days <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Last 7 Days</a></li>
                                <li><a href="#">Last 6 Days</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="dashboard-box-wrapper">
                    <div class="dashboard-box-inner">
                        <div class="dashboard-box">
                            <div class="row">
                                <div class="col-sm-10">
                                    <p>Customer Bookings</p>
                                </div>
                                <div class="col-sm-2 text-right"><span class="border-round"></span><span class="border-round fill"></span></div>
                            </div>
                            <div id="CustomerChart" class="m-t-20"></div>
                        </div>
                    </div>
                    <div class="box-border">
                        <div class="dropdown more-link">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Last 30 Days <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Last 7 Days</a></li>
                                <li><a href="#">Last 6 Days</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="dashboard-box-wrapper">
                    <div class="dashboard-box-inner">
                        <div class="dashboard-box">
                            <div class="row">
                                <div class="col-sm-10">
                                    <p>Agent Bookings</p>
                                </div>
                                <div class="col-sm-2 text-right"><span class="border-round fill"></span><span class="border-round"></span></div>
                            </div>
                            <div id="AgentChart" class="m-t-20"></div>
                        </div>
                    </div>
                    <div class="box-border">
                        <div class="dropdown more-link">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Last 30 Days <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Last 7 Days</a></li>
                                <li><a href="#">Last 6 Days</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    </section>

    <section class="m-t-20">
        <h2 class="page-title">Google Analytics Overview</h2>
        <div class="row m-t-30">
            <div class="col-md-8">
                <div class="analytics-wrapper">
                    <div class="analytics-inner">
                        <img src="{{ url('assets/images/graph.png') }}" alt="" class="img-responsive" />
                    </div>

                    <div class="row box-border">
                        <div class="col-sm-5">
                            <div class="dropdown more-link">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Last 7 Days <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Last 7 Days</a></li>
                                    <li><a href="#">Last 6 Days</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-7 right-link">
                            <a href="#">Audience overview <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-wrapper">
                    <div class="card-inner">
                        <p>Users right now</p>
                        <h2>0</h2>
                        <div class="m-t-20">
                            <input type="text" name="" class="input-control" placeholder="Page views per minute" />
                        </div>
                        <div class="m-t-80">
                            <div class="row">
                                <div class="col-sm-10 padding-right-0">
                                    <input type="text" name="" class="input-control" placeholder="Top Active Pages" />
                                </div>
                                <div class="col-sm-2 padding-left-0">
                                    <input type="text" name="" class="input-control" placeholder="Users" />
                                </div>
                            </div>
                        </div>
                        <p class="m-t-10 border-bottom-light">There is no data for this view.</p>
                    </div>
                    <div class="box-border right-link">
                        <a href="#">Real-Time Report <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="m-t-20">
        <h2 class="page-title">How do you acquire users?</h2>
        <div class="m-t-30">
            <div class="users-wrapper">
                <div class="users-inner">
                    <div id="usersChart"></div>
                </div>
                <div class="row box-border">
                    <div class="col-sm-5">
                        <div class="dropdown more-link">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Last 7 Days <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Last 7 Days</a></li>
                                <li><a href="#">Last 6 Days</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-7 right-link">
                        <a href="#">Acquisition Report <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="m-t-20">
        <div class="row">
            <div class="col-md-6 dashboard-box7">
                <h2 class="page-title">How are your active users trending over time?</h2>
                <div class="m-t-30">
                    <div class="users-wrapper">
                        <div class="users-inner">
                            <p class="gray-label">Active Users</p>
                            <div id="activeUserChart" class="chart"></div>
                        </div>
                        <div class="box-border">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="dropdown more-link">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Last 30 Days <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Last 7 Days</a></li>
                                            <li><a href="#">Last 6 Days</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-sm-7 right-link">
                                    <a href="#">Active Users Report <i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 dashboard-box8">
                <h2 class="page-title">How well do you retain users?</h2>
                <div class="m-t-30">
                    <div class="users-wrapper">
                        <div class="users-inner">
                            <p class="gray-label">User retention</p>
                            <div id="userRetentionChart" class="chart"></div>
                        </div>
                        <div class="box-border">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="dropdown more-link">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Last 6 weeks <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Last 7 Days</a></li>
                                            <li><a href="#">Last 6 Days</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-sm-7 right-link">
                                    <a href="#">Cohort Analysis Report <i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="m-t-20">
        <div class="row">
            <div class="col-sm-4">
                <h2 class="page-title">When do your users visit?</h2>
                <div class="dashboard-box-wrapper m-t-30">
                    <div class="dashboard-box-three">
                        <p class="gray-label">Users by time of day</p>
                        <div>
                            <img src="{{ url('assets/images/graph1.png') }}" alt="" class="img-responsive" />
                        </div>
                    </div>
                    <div class="box-border">
                        <div class="dropdown more-link">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Last 30 Days <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Last 7 Days</a></li>
                                <li><a href="#">Last 6 Days</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <h2 class="page-title">Where are your users?</h2>
                <div class="dashboard-box-wrapper m-t-30">
                    <div class="dashboard-box-three">
                        <p class="gray-label">Session by country</p>
                        <div id="geoChart" class="m-t-20"></div>
                    </div>
                    <div class="row box-border">
                        <div class="col-sm-5">
                            <div class="dropdown more-link">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Last 30 Days <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Last 7 Days</a></li>
                                    <li><a href="#">Last 6 Days</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-7 right-link">
                            <a href="#">Location Overview <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <h2 class="page-title">What are your top devices?</h2>
                <div class="dashboard-box-wrapper m-t-30">
                    <div class="dashboard-box-three">
                        <p class="gray-label">Session by device</p>
                        <div id="device-chart"></div>
                    </div>
                    <div class="row box-border">
                        <div class="col-sm-5">
                            <div class="dropdown more-link">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Last 30 Days <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Last 7 Days</a></li>
                                    <li><a href="#">Last 6 Days</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-7 right-link">
                            <a href="#">Mobile Overview <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    </div>
    </div>
@stop

@section('custom-js')

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

google.charts.load("current", {
 	packages: ["corechart"]
});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
 	var data = google.visualization.arrayToDataTable([
		['Task', ''],
		['Pending', 20],
		['Booked', 60],
		['Cancelled', 20]
 	]);

 	var options = {
		title: '',
		pieHole: 0.5,
		colors: ['#76DDFB', '#2C82BE', '#DBECF8'],
		legend: {
		 position: 'top'
		},
 	};

 	var chart = new google.visualization.PieChart(document.getElementById('CustomerChart'));
 		chart.draw(data, options);
}

google.charts.load("current", {
 	packages: ["corechart"]
});
google.charts.setOnLoadCallback(drawChart1);

function drawChart1() {
	var data = google.visualization.arrayToDataTable([
		['Task', 'Hours per Day'],
		['Pending', 16],
		['Booked', 60],
		['Cancelled', 20],
		['', 4]
	]);

 	var options = {
		title: '',
		pieHole: 0.5,
		colors: ['#76DDFB', '#2C82BE', '#DBECF8', '#2C82BE'],
		legend: {
		 position: 'top'
		},
 	};

 	var chart = new google.visualization.PieChart(document.getElementById('AgentChart'));
 		chart.draw(data, options);
	}

google.charts.load('current', {
 	'packages': ['line']
});
google.charts.setOnLoadCallback(drawChart2);

function drawChart2() {

	var data = new google.visualization.DataTable();
	data.addColumn('number', '');
	data.addColumn('number', 'Monthly');
	data.addColumn('number', 'Weekly');
	data.addColumn('number', 'Daily');


 	data.addRows([
		[0, 37.8, 80.8, 41.8],
		[2, 30.9, 69.5, 32.4],
		[3, 25.4, 57, 25.7],
		[4, 11.7, 18.8, 10.5]
 	]);

 	var options = {
		chart: {
		 title: ''
		},
		colors: ['#2F64D9', '#3E82F7', '#79A8FA'],
		axes: {
		 x: {
		     0: {
		         side: 'bottom'
		     }
		 },
		 y: {
		     0: {
		         side: 'right'
		     }
		 }
		},
 	};

 	var chart = new google.charts.Line(document.getElementById('activeUserChart'));

 		chart.draw(data, options);
}

google.charts.load("current", {
 	packages: ["corechart"]
});
google.charts.setOnLoadCallback(drawChart3);

function drawChart3() {
	var data1 = google.visualization.arrayToDataTable([

		['Quarks', 'Leptons', 'Gauge Bosons', 'Scalar Bosons'],
		[2 / 3, -1, 0, 0],
		[2 / 3, -1, 0, null],
		[2 / 3, -1, 0, null],
		[-1 / 3, 0, 1, null],
		[-1 / 3, 0, -1, null],
		[-1 / 3, 0, null, null],
		[-1 / 3, 0, null, null]

	]);

	var options1 = {
		title: '',
		legend: {
		 position: 'none'
		},
		x: {
		 0: {
		     side: 'top'
		 }
		},
		colors: ['#2F64D9', '#E0E0E0']
	};

	var chart1 = new google.visualization.Histogram(document.getElementById('userRetentionChart'));
	chart1.draw(data1, options1);
}

google.charts.load('current', {
 	packages: ['corechart', 'bar']
});
google.charts.setOnLoadCallback(drawAxisTickColors);

function drawAxisTickColors() {
	var data = google.visualization.arrayToDataTable([
		[' ', 'Direct', 'Organic Search', 'Social', 'Other', {
		role: 'annotation'
		}],
		['21', 10, 24, 20, 32, ''],
		['22', 16, 22, 23, 30, ''],
		['23', 28, 19, 29, 30, '']
	]);

	var options = {
		height: 300,
		legend: {
		position: 'bottom',
		maxLines: 3
	},
	bar: {
		groupWidth: '75%'
	},
	isStacked: true,
		colors: ['#2F64D9', '#70A2FA', '#9FC1FB', '#CFDFFD'],
	};

	var chart = new google.visualization.ColumnChart(document.getElementById('usersChart'));
	chart.draw(data, options);
}

google.charts.load('current', {
 	'packages': ['geochart'],
 	// Note: you will need to get a mapsApiKey for your project.
 	// See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
 	'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
});
google.charts.setOnLoadCallback(drawRegionsMap);

function drawRegionsMap() {
	var data = google.visualization.arrayToDataTable([
		['Country', 'Popularity'],
		['Germany', 200],
		['United States', 300],
		['Brazil', 400],
		['Canada', 500],
		['France', 600],
		['RU', 700]
	]);

	var options = {
		colors: ['#DDDDDD', '#3E82F7', '#CFDFFD']
	};

	var chart = new google.visualization.GeoChart(document.getElementById('geoChart'));

	chart.draw(data, options);
}

google.charts.load("current", {
 	packages: ["corechart"]
});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
	var data = google.visualization.arrayToDataTable([
		['Task', ''],
		['Pending', 20],
		['Booked', 60],
		['Cancelled', 20]
	]);

	var options = {
		title: '',
		pieHole: 0.5,
		colors: ['#76DDFB', '#2C82BE', '#DBECF8'],
		legend: {
		 position: 'top'
		},
	};

	var chart = new google.visualization.PieChart(document.getElementById('CustomerChart'));
	chart.draw(data, options);
}

google.charts.load("current", {
 	packages: ["corechart"]
});
google.charts.setOnLoadCallback(drawChart4);

function drawChart4() {
	var data = google.visualization.arrayToDataTable([
		['Task', ''],
		['Desktop', 16],
		['Mobile', 60]
	]);

	var options = {
		title: '',
		pieHole: 0.6,
		colors: ['#2F64D9', '#3E82F7'],
		legend: {
		 	position: 'none'
		},
	};

	var chart = new google.visualization.PieChart(document.getElementById('device-chart'));
	chart.draw(data, options);
} 
</script>

@stop