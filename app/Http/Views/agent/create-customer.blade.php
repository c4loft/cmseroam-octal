@extends('layout/mainlayout')
@section('content')
@php
	$interests = array(
	  15=>'4WD',6=>'Astronomy',18=>'Backpacking',8=>'Urban-Adventures',
	  5=>'Ballooning', 12=>'Camping',9=>'Overland', 1=>'Expeditions',
	  13=>'Festivals', 24=>'Cycling',3=>'Family',23=>'Fishing',
	  29=>'HorseRiding',28=>'Kayaking',19=>'Wildlife',4=>'Short-Breaks',
	  11=>'Sailing', 16=>'ScenicFlight',17=>'Sky-Diving',2=>'Snorkelling',
	  20=>'Surfing', 14=>'Walking-Trekking', 7=>'Polar', 10=>'Food'
	  );
@endphp
<div class="content-container">
    <h1 class="page-title">Create New Customer</h1>

    <div class="alert" role="alert" id="error_msg"></div>
    <form class="add-form" method="post" action="{{ route('agent.store-customer') }}">
    {{ csrf_field() }}
    <div class="box-wrapper">
    	<p>Personal Information</p>
		<div class="row">
			<div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">Account ID</label>
                    <input type="text" name="account_id" id="account_id" class="form-control" placeholder="Account ID" required="true">
                </div>      
            </div>
            <div class="col-sm-6">
				<div class="form-group">
					<label class="label-control">Name</label>
					<input type="text" name="name" id="name" class="form-control" placeholder="Name">
				</div>		
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label class="label-control">Family Name</label>
					<input type="text" name="family_name" id="family_name" class="form-control" placeholder="Family Name">
				</div>		
			</div>
		</div>
		<div class="form-group">
			<label class="label-control">Gender</label>
			<select name="gender" id="gender" class="form-control">
				<option value="">Select Gender</option>
				<option value="Male">Male</option>
				<option value="Female">Female</option>
				<option value="Other">Other</option>
			</select>
		</div>	
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label class="label-control">Phone</label>
					<input type="text" name="contact_no" id="contact_no" class="form-control" placeholder="Phone">
				</div>		
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label class="label-control">Email</label>
					<input type="email" name="email" id="email" class="form-control" placeholder="Email">
				</div>		
			</div>
		</div>	
		<div class="form-group">
			<label class="label-control">Contact Method</label>
			<select name="contact_method" id="contact_method" class="form-control">
				<option value="">Select Contact Method</option>
				<option value="Phone">Phone</option>
				<option value="SMS">SMS</option>
				<option value="Email">Email</option>
				<option value="Postal">Postal</option>
				<option value="Carrier Pigeon">Carrier Pigeon</option>
			</select>
		</div>
        <div class="form-group">
            <label class="label-control">Domain</label>

            <select name="domain_id" id="domain_id" class="form-control">
                <option value="">Select Domain</option>
                @foreach($agent_domains as $domain)
                    <option value="{{ $domain['domains']['id'] }}">{{ $domain['domains']['name'] }}</option>
                @endforeach
            </select>
        </div>	
    </div>
    <div class="box-wrapper">
    	<p>Personal Preferences</p>
    	<div class="form-group">
    		<label class="label-control"> Frequently Used Products</label>
    		<select name="freq_used_products[]" id="freq_used_products"  multiple="multiple" class="form-control freq_product" placeholder="Select Frequently Used Product">
    			<option value="Flight">Flight</option>
    			<option value="Accommodation">Accommodation</option>
    			<option value="Transfer">Transfer</option>
    			<option value="Insurance">Insurance</option>
    			<option value="Cruise">Cruise</option>
    			<option value="Car-Hire">Car-Hire</option>
    			<option value="Activities">Activities</option>
    			<option value="Tours">Tours</option>
    		</select>
    	</div>
    	<p>Accommodation</p>
    	<div class="row">
    		<div class="col-sm-6">
		    	<div class="form-group">
		    		<label class="label-control"> Accommodation Type </label>
		    		<select name="accommodation_type" id="accommodation_type" class="form-control" placeholder="Select Accommodation Type">
		    			<option value="1"> 1 Star</option>
						<option value="5"> 2 Star</option>
						<option value="2"> 3 Star</option>
						<option value="3"> 4 Star</option>
						<option value="9"> 5 Star</option>
		    		</select>
		    	</div>
    		</div>
    		<div class="col-sm-6">
    			<div class="form-group">
    				<label class="label-control"> Room Type </label>
    				<select class="form-control" name="room_type" id="room_type">
    					<option value="">Select Accommodation Type</option>
    					<option value="23">Smoking</option>
    					<option value="24">Non-Smoking</option>
    				</select>
    			</div>
    		</div>
    	</div>
    	
    	<p>Transport</p>
    	<div class="row">
    		<div class="col-sm-6">
    			<div class="form-group">
    				<label class="label-control">Transport Type</label>
    				<select class="form-control" name="transport_type" id="transport_type">
    					<option value="">Select Transport Type</option>
    					<option value="1">Flight</option>
    					<option value="2">Overland</option>
    				</select>
    			</div>
    		</div>
    		<div class="col-sm-6">
		    	<div class="form-group">
		    		<label class="label-control">Cabin Class</label>
		    		<select class="form-control" name="cabin_class" id="cabin_class">
						<option value="">Select Cabin Class</option>
						<option value="Y">Economy</option>
						<option value="C">Business</option>
						<option value="F">First</option>
						<option value="S">Premium Economy</option>
				   	</select>
		    	</div>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-sm-6">
    			<div class="form-group">
    				<label class="label-control">Seat Type</label>
    				<select class="form-control seat_type" name="seat_type[]" id="seat_type" multiple="multiple">
    					<option value="">Select Seat Type</option>
    					<option value="Window">Window</option>
    					<option value="Aisle">Aisle</option>
    					<option value="Front">Front</option>
    					<option value="Back">Back</option>
    				</select>
    			</div>
    		</div>
    		<div class="col-sm-6">
		    	<div class="form-group">
		    		<label class="label-control">Meal Type</label>
		    		<select class="form-control meal_type" name="meal_type[]" id="meal_type" multiple="multiple">
    					<option value="">Select Meal Type</option>
    					<option value="Vegan">Vegan</option>
    					<option value=" Non Vegetarian"> Non Vegetarian</option>
    					<option value="Diabetic">Diabetic</option>
    					<option value="Non-Lactose">Non-Lactose</option>
    					<option value="Fruit Platter">Fruit Platter</option>
    					<option value="Gluten Free">Gluten Free</option>
    					<option value="Low Fat">Low Fat</option>
    					<option value="Raw Vegetable">Raw Vegetable</option>
    				</select>
		    	</div>
    		</div>
    	</div>
    	<p>Activities</p>
    	<div class="form-group">
    		<label class="label-control">Activity / Tour Type</label>
    		<select name="interests[]" id="interests"  multiple="multiple" class="form-control interests" placeholder="Select Interests">
    			@foreach($interests as $key => $interest) 
    				<option value="{{ $key }}">{{ $interest }}</option>
    			@endforeach
    		</select>
    	</div>
    </div>
    <div class="box-wrapper">
    	<p>Conversation Notes</p>
    	<div class="form-group">
            <label class="label-control">Conversation Notes</label>
    		<textarea name="additional_info" id="additional_info" class="form-control"></textarea>
    	</div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="submit" class="btn btn-primary">Save</button>
        </div>
    </div>
    </form>
</div>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ asset('assets/js/agent.js') }}"></script>
<script type="text/javascript">
    $(".freq_product,.seat_type,.meal_type,.interests").selectize();
</script>
@endpush