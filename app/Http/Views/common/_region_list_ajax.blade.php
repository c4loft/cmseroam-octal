@if((auth::user()->type) !="admin"&& (auth::user()->type) !="eroamProduct")
<div class="form-group">
  <label for="usr">Your Domains:</label>
	<select class="form-control"  onChange="selectDomainForGeoData(this.value)" name="user_domains">
				<option  value="">Choose Domain</option>
		@if(isset($user_domain) && $user_domain->count() > 0)
				@foreach($user_domain as $key=>$domains)
				<option id="{{$domains->domain_id}}" value="{{$domains->domain_id}}">{{domianName($domains->domain_id)->name}}</option>
				@endforeach
		@endif
	</select>
</div>
@elseif((auth::user()->type) !="admin"&& (auth::user()->type) =="eroamProduct")
<div class="col-xs-6 form-group">
  <label for="usr">Your Licence:</label>
	<select class="form-control" onChange="getDomainsbyLicenseId(this.value);" >
				<option  value="">Choose Licence</option>
		@if(isset($licensee) && count($licensee) > 0)
				@foreach($licensee as $key=>$value)
				<option id="{{$key}}" value="{{$key}}">{{$value}}</option>
				@endforeach
		@endif
	</select>
</div>

<div class="col-xs-6 form-group">
  <label for="usr">Your Domains:</label>
	<select class="form-control"  onChange="selectDomainForGeoData(this.value)" id="Domains" name="user_domains">
			<option  value="">Choose Domain</option>
	</select>
</div>
@endif