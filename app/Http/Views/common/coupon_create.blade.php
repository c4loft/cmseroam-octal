@extends( 'layout/mainlayout' )

@section('custom-css')
<style>
    .error {
        color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }

    .error_message{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    div .with_error{
        border:1px solid black;
    }
</style>
@stop

@section('content')

<div class="content-container">
    @if($nIdCoupon == '')
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Coupon']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Coupon']) }}</h1>
    @endif

    @if (Session::has('message'))
    <div class="small-12 small-centered columns success_message">{{ Session::get('message') }}</div>
    <br>
    @endif

    @if ($errors->any())
    <div class=" error_message">{{$errors->first()}}</div>
    @endif
    @if($nIdCoupon == '')
        {{ Form::open(array('id' => 'addCouponForm', 'url' => 'common/create-coupon','method'=>'Post')) }}
    @else
        {{ Form::model($oCoupon,array('id' => 'addCouponForm', 'url' => 'common/create-coupon','method'=>'Post')) }}
    @endif
    
    <div class="box-wrapper">
        <div class="form-group">
            <label class="label-control">Title <span class="required">*</span></label>
            <?php $attributes = 'form-control' ?>
            {{Form::text('title',Input::old('title'),['placeholder'=>'Title','id'=>'title','class'=>$attributes])}}
        </div>
        <input type="hidden" name="coupon_id" value="{{ $nIdCoupon }}" />
        <div class="form-group">
            <label class="label-control">Code <span class="required">*</span></label>
            {{Form::text('code',Input::old('code'),['placeholder'=>'Code','id'=>'code','class'=>$attributes])}}
        </div>


        <div class="form-group">
            <label class="label-control">StartDate <span class="required">*</span></label>
            {{Form::text('start_date',Input::old('start_date'),['placeholder'=>'StartDate','id'=>'start_date','class'=>$attributes])}}
        </div>

        <div class="form-group">
            <label class="label-control">EndDate <span class="required">*</span></label>
            {{Form::text('end_date',Input::old('end_date'),['placeholder'=>'EndDate','id'=>'end_date','class'=>$attributes])}}
        </div>
        <div class="form-group">
            <label class="label-control">Coupon Type <span class="required">*</span></label>
            <?php $coupon_type = ['' => 'Select Coupon Type', 'percentage' => 'Percentage', 'flat' => 'Flat']; ?>
            {{Form::select('coupon_type',$coupon_type, Input::old('coupon_type'),['id'=>'coupon_type','class'=>$attributes])}}
        </div>


        
        <div class="form-group" id="currencyDiv">
            <label class="label-control">Currency <span class="required">*</span></label>
            {{Form::select("coupon_currency",$oCurrencies,Input::old("coupon_currency"),["id"=>"coupon_currency","class"=>$attributes])}}
        </div>

        <div class="form-group">
            <label class="label-control">Amount <span class="required">*</span></label>
            {{Form::text('amount',Input::old('amount'),['placeholder'=>'Amount','id'=>'amount','class'=>$attributes])}}
        </div>  
    </div>
    <div class="m-t-20 row">
        <div class="col-sm-6">
            {{Form::submit('Add Coupon',['class'=>'btn btn-primary btn-block','name'=>'btnAddCoupon', 'id'=>'btnAddCoupon']) }}
        </div>
        <div class="col-sm-6">
            <a href="{{URL::previous()}}" class="btn btn-primary btn-block">Cancel</a>
        </div>
    </div>
    {{ Form::close() }}
</div>
@stop

@section('custom-js')
<script>
    $(document).ready(function () {

        $("#start_date").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
        }).on('changeDate', function (selected) {
            var minDate = new Date(selected.date.valueOf());
            $('#end_date').datepicker('setStartDate', minDate);
        });
        var sCoupon = $('#coupon_type').val();
        if(sCoupon == 'flat')
            $('#currencyDiv').show();
        else
            $('#currencyDiv').hide();
        console.log(sCoupon);
        $("#end_date").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (selected) {
            var maxDate = new Date(selected.date.valueOf());
            $('#start_date').datepicker('setEndDate', maxDate);
        });

        $('#coupon_type').change(function () {
            var type = $(this).val();
            if (type == 'flat') {
                $("#currencyDiv").show();
                $("#amount").rules("remove", "range");
                $("#coupon_currency").rules("add", {required: true});
            } else {
                $("#currencyDiv").hide();
                $("#amount").rules("add", {range: [0, 100]});
                $("#coupon_currency").rules("remove");
            }
        });

        $("#addCouponForm").validate({
            rules: {
                title: "required",
                code: "required",
                start_date: "required",
                end_date: "required",
                coupon_type: "required",
                amount: {required: true, number: true},
            },
            errorPlacement: function (label, element) {
                label.insertAfter(element);
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    });
</script>
@stop
