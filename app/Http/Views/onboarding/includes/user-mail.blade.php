<?php $type = ucwords(str_replace("_", ' ', $type));
  ?>
<body style="margin: 0px; background: #e3e5e7;">
    <table width="100%" style="background: #e3e5e7">
        <tr>
            <td>
                <table style="margin:0 auto; background: #ffffff;" width="760px" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th style="background-color: #212121; padding-top:20px; padding-bottom: 20px; text-align: center;">
                                <a href="#"><img src="https://eroam-dev.s3.us-west-2.amazonaws.com/onboardingLogo/{{$logo}}".$logo" width="180px" alt="" /> </a>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="background: #fafafa; border-bottom: 1px solid #f0f0f0; padding-top: 5px; padding-bottom: 5px;">
                                <table width="100%">
                                    <tr>
                                        <td style="text-align: center">
                                            <p style="font-family: Arial; font-size: 18px; color: #212121; margin-top: 0px; margin-bottom: 0px;  "><strong> Hi, {{ $name }}!</strong></p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="90%;" style="margin: 0 auto;">
                                    <tbody>
                                      
										<tr><td style="font-family: Arial; font-size: 14px; padding-top: 40px;">
        We would like to welcome you to {{ $domain }} and let you know your account has been created as {{ $type }}.
</td></tr>
									
<tr><td style="font-family: Arial; font-size: 14px; padding-top: 20px;">Below you will find your user credentials and a quick link to log into your account.
</td></tr>
<tr><td style="font-family: Arial; font-size: 14px; padding-top: 20px;">
User Type: {{ $type }}
</td></tr>
<tr><td style="font-family: Arial; font-size: 14px; padding-top: 20px;">
User ID: {{ $email }}
</td></tr>
<tr><td style="font-family: Arial; font-size: 14px; padding-top: 20px;">
Password: {{ $password }}
</td></tr>
                                        
<tr><td style="font-family: Arial; font-size: 14px; padding-top: 40px;">Log Into 
<a href="{{ $domain }}" target="_blank" style="background-color: white;color: black;padding: 14px 28px;font-size: 16px;cursor: pointer;background-color: #4CAF50;color: white;">Click Here</a></td></tr>                                     

										<tr><td style="font-family: Arial; font-size: 14px; padding-top: 40px;">Thanks,<br>
                                        The {{ $domain }} Team</td></tr>
								
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td style="background-color: #394951; padding-top:6px; padding-bottom: 6px; padding-right: 6px; padding-left: 6px; text-align: center;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="font-family: Arial; font-size: 14px;"> <a href="#"><img src="https://eroam-dev.s3.us-west-2.amazonaws.com/onboardingLogo/{{$logo}}" alt="" /> </a></td>
                                        <td style="color: #fff;font-size: 12px; text-align: right; font-family: Arial; ">Powered by eRoam &copy; Copyright 2018 - 2019. All Rights Reserved. Patent pending AU2016902466</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </td>
        </tr>
    </table>
</body>
