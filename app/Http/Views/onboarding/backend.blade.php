@extends( 'layout/mainlayout' )
@section('content')
<div class="content-container">
    <div class="m-t-10">
        <?php
        if(isset($licensee_id) && $licensee_id!=''){ $licensee_id = $licensee_id; } else { $licensee_id = ''; } 
        ?>
        @include('WebView::onboarding.includes.onboarding-steps', array('id' => $licensee_id))
    </div>

    <div class="alert" role="alert" id="error_msg"></div>
    <h1 class="page-title">Step 3: Back-End Configuration</h1>

    <div class="formbox">
        <?php $x=1; ?>
        @if($total < 1)
        <form class="add-form" method="post" id="backend-form" action="{{route('backend.add')}}">
            {{ csrf_field() }}
           
            <input type="hidden" name="licensee_id" value="{{ $licensee_id }}">
            <div class="box-wrapper">
                 
                <a data-toggle="collapse" data-parent="#accordion" href="#firstdomain"><p>Consumer Back-End</p></a>
                <div class="collapse" id="firstdomain">
                @foreach($result as $key => $value)
                <div class="box-wrapper m-t-20">

                    <p>{{ $key }}</p>
                    <div class="row">
                        @foreach($value as $arr)
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="radio-checkbox label_check m-t-10 @if($arr['status'] == 0) disabled @endif @if(isset($domain_backend) && in_array($arr['id'], $domain_backend)) c_on @endif" for="checkbox-{{ $arr['id'].$x }}"><input type="checkbox" id="checkbox-{{ $arr['id'].$x }}" name="domain_backend[]" value="{{ $arr['id'] }}" @if($arr['status'] == 0) disabled @endif  >{!! $arr['item'] !!}</label>
                            </div>
                        </div>

                        @if($loop->iteration%2 === 0)
                    </div>
                    <div class="row">
                        @endif
                        @endforeach
                    </div>
                </div>
                @endforeach
                <div class="error_here"></div>
                @if(isset($domain_backend))
                <div class="m-t-20 row">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="row">
                            <div class="col-sm-6">
                                <button type="sumbit" name="" class="btn btn-primary btn-block">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                </div>
            </div>            
        </form>
        @endif


        
        @foreach($domain_ids as $domain_id)

        <form action="@if(isset($domain_backend) && count($domain_backend) > 0 ){{route('backend.update',[$licensee_id])}}@else{{route('backend.add')}}@endif" class="add-form" method="post" id="backend-form">
            {{ csrf_field() }}
            @if(isset($domain_backend) && count($domain_backend) > 0 )
                @method('PUT')
            @endisset 
            <input type="hidden" name="licensee_id" value="{{ $licensee_id }}">
            <input type="hidden" name="domain_id" value="{{ $domain_id }}">
            <div class="box-wrapper">
                 
                <a data-toggle="collapse" data-parent="#accordion" href="#firstdomain{{$x}}"><p>Consumer Back-End</p></a>
                <div class="collapse" id="firstdomain{{$x}}">
                @foreach($result as $key => $value)
                <div class="box-wrapper m-t-20">

                    <p>{{ $key }}</p>
                    <div class="row">
                        @foreach($value as $arr)
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="radio-checkbox label_check m-t-10 @if($arr['status'] == 0) disabled @endif @if(isset($domain_backend) && in_array($arr['id'], $domain_backend)) c_on @endif" for="checkbox-{{ $arr['id'].$x }}"><input type="checkbox" id="checkbox-{{ $arr['id'].$x }}" name="domain_backend[]" value="{{ $arr['id'] }}" @if($arr['status'] == 0) disabled @endif 
                                    <?php
                                    foreach ($domain_backends as $key => $val) {
                                        if(($val['domain_id'] == $domain_id) && ($val['config_id'] == $arr['id'])){
                                            echo 'checked';
                                        }
                                    }
                                    ?>
                                >{!! $arr['item'] !!}</label>
                            </div>
                        </div>

                        @if($loop->iteration%2 === 0)
                    </div>
                    <div class="row">
                        @endif
                        @endforeach
                    </div>
                </div>
                @endforeach
                <div class="error_here"></div>
                @if(isset($domain_backend))
                <div class="m-t-20 row">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="row">
                            <div class="col-sm-6">
                                <button type="sumbit" name="" class="btn btn-primary btn-block">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                </div>
            </div>            
        </form>
        <?php $x++; ?>
        @endforeach
        <div class="m-t-20 row">
            <div class="col-sm-offset-2 col-sm-8">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="{{ route('onboarding.frontend',[$licensee_id]) }}" class="btn btn-primary btn-block">Previous</a>
                    </div>
                    <div class="col-sm-6">
                        @if(isset($domain_backend))
                        <a href="{{ route('onboarding.geodata',[$licensee_id]) }}" class="btn btn-primary btn-block">Next</a>
                        @else
                        <button type="sumbit" name="" class="btn btn-primary btn-block">Next</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ asset('assets/js/blockui.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/onboarding.js') }}"></script>
@endpush