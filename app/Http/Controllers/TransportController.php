<?php
//priya
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use App\Transport;
use App\TransportOperator;
use App\TransportPassengerType;
use App\TransportPrice;
use App\TransportBasePrice;
use App\TransportSupplier;
use App\TransportType;
use App\TransportMarkup;
use App\TransportMarkupSupplierCommission;
use App\TransportMarkupAgentCommission;
use App\TransportMarkupPercentage;
use App\Country;
use App\City;
use App\Currency;
use App\PassengerType;
use App\Airline;
use App\Domain;
use App\Licensee;

use Auth;
use Session;
use File;
use Image;
use DB;

class TransportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function callTransportList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'transport' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('transport');

        session(['page_name' => 'transport']);
        $aData = session('transport') ? session('transport') : array();

        $oRequest->session()->forget('transport');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 't.id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
		//$user_type=Auth::user()->type;
		//echo $user_type;die;
        $oTransportList = Transport::getTransportList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
		
		//pr($oTransportList);die;
		
        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oTransportList->currentPage(),'transport');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::transport._transport_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::transport.transport_list' : 'WebView::transport._transport_list_ajax';
        
        return \View::make($oViewName, compact('oTransportList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));  
    }
    
    public function callTransportDelete($nId)
    {
        $oTransport = Transport::find($nId);
        $oTransport->delete();
        return 1;
    }
    
    public function callTransportCreate(Request $oRequest,$nId='') 
    {
		//pr(Auth::user());die;
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'de_countries' => 'required',
                                    'from_city_id' => 'required',
                                    #'address_from' => 'required',
                                    'dn_countries' => 'required',
                                    'to_city_id' => 'required',
                                    #'address_to' => 'required',
                                    'transport_type_id' => 'required',
                                    'currency_id' => 'required',
                                    'default_transport_supplier_id'=> 'required',
                                    //'phone'=> 'numeric|regex:/[0-9]{10}/',
                                    'operator_id' => 'required'
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }

			if( Auth::user()->type == 'eroamProduct' && $oRequest->id != ''){
				//echo "dd";die;
				
				 $aData = $oRequest->except(['id','_token','de_countries','dn_countries']);
			
				$aData['[default]'] = $aData['default'] ?? '0';
				
				$aData['created_by'] = Auth::user()->id;
			
				$oTransport = Transport::updateOrCreate(['id'=>$oRequest->id],$aData);
				
			}else{
				//echo "aaa";die;
				
				$domains = '';
				if($oRequest->domains){
					$domains = implode(',',$oRequest->domains);
				}
				$aData = $oRequest->except(['id','_token','de_countries','dn_countries']);
				$aData['domain_id'] = $domains;
				$aData['license_id'] = Auth::user()->licensee_id;
				$aData['user_id'] = Auth::user()->id;
				$aData['[default]'] = $aData['default'] ?? '0';
				$aData['created_by'] = Auth::user()->id;
				//pr($aData);die;
				$oTransport = Transport::updateOrCreate(['id'=>$oRequest->id],$aData);
				
			}

            if($oRequest->id != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            return redirect()->route('transport.transport-list',['nId'=>$oTransport->id,'nFrom' => 1]);
			
        }
		
        $oCountries = Country::orderBy('name','asc')->pluck('name','id');
        $aTransportType = TransportType::orderBy('name','asc')->pluck('name','id');
        $aTransportOperator = TransportOperator::orderBy('name','asc')->pluck('name','id');
        $aTransportSupplier = TransportSupplier::orderBy('name','asc')->pluck('name','id');
        $oCurrencies  = Currency::orderBy('code','asc')->pluck('code','id');
		$licensee = Licensee::orderBy('business_name','asc')->pluck('business_name','id');
        $allDomain=Domain::join('user_domains', 'user_domains.domain_id', '=', 'domains.id')
        ->where('user_domains.user_id', Auth::user()->id)  
        ->select(
           'domains.name as name',
           'user_domains.domain_id as id'
       )->orderBy('user_domains.domain_id')->get(); 
		
        if($nId != '')
        {
            $oTransport = Transport::find($nId);
            $oTransport->from_city_id;
            $odeCo = City::find($oTransport->from_city_id);
            $odnCo = City::find($oTransport->to_city_id);
            $oTransport['de_countries'] = $odeCo->country_id;
            $oTransport['dn_countries'] = $odnCo->country_id;
			$domain = Domain::select('id', 'name')->where('licensee_id',$oTransport->license_id)->get();
        }
        return \View::make('WebView::transport.transport_create',compact('oCountries','aTransportType','aTransportOperator','aTransportSupplier','oTransport','nId','oCurrencies','licensee_id','allDomain','licensee','domain'));
    }
	
    public function callTransportSupplierList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'ts_supplier' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('ts_supplier');

        session(['page_name' => 'ts_supplier']);
        $aData = session('ts_supplier') ? session('ts_supplier') : array();

        $oRequest->session()->forget('ts_supplier');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : 'name');
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 't.id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oTransportList = TransportSupplier::getTransportSupplierList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oTransportList->currentPage(),'ts_supplier');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::transport._transport_supplier_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::transport.transport_supplier_list' : 'WebView::transport._transport_supplier_list_ajax';
        
        return \View::make($oViewName, compact('oTransportList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));  
    }
    
    public function callTransportSupplierDelete($nId) 
    {
        $oTransport = TransportSupplier::find($nId);
        $oTransport->delete();
        return 1;
    }
    
    public function callTransportSupplierCreate(Request $oRequest,$nId = '')
    {
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'name' => 'required',
                                    'marketing_contact_title' => 'required',
                                    'marketing_contact_name' => 'required',
                                    'marketing_contact_email' => 'required',
                                    'marketing_contact_phone' => 'required',
                                    'reservation_contact_name' => 'required',
                                    'reservation_contact_email' => 'required',
                                    'reservation_contact_landline' => 'required',
                                    'reservation_contact_free_phone' => 'required',
                                    'accounts_contact_name' => 'required',
                                    'accounts_contact_email' => 'required',
                                    'accounts_contact_title' => 'required',
                                    'accounts_contact_phone' => 'required',
                                    'logo' => 'required_without:id|image|dimensions:max_width=500,max_height=500|mimes:jpeg,png,jpg',
                                    'description' => 'required',
                                    'special_notes' => 'required',
                                    'remarks' => 'required',
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            
            $aData = $oRequest->except([ 'id','_token' ]);
            if(Input::hasFile('logo')){
                    $image   = Input::file('logo');
                    $image_validator = image_validator($image);
                // check if image is valid
                if( $image_validator['fails'] ){
                    //$error = ['message' => $image_validator['message']];
                }else{
                    $imageName = time().'.'.$image->getClientOriginalExtension(); 
                    $filePath = 'transport-suppliers/' . $imageName;
                    $t = \Storage::disk('s3')->put($filePath, file_get_contents($image), 'public');                   
                    $update['mapfile'] = $imageName;
                    $aData['logo'] = $filePath;
                }
            }    
            //insert data
            $oTransportSupplier = TransportSupplier::updateOrCreate(['id' => $oRequest->id],$aData);
            
            if($oRequest->id != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            return Redirect::route('transport.transport-supplier-list');
        }
        $oTransportSupplier = TransportSupplier::where('id',$nId)->first();
        return \View::make('WebView::transport.transport_supplier_create',compact('nId','oTransportSupplier'));
        
    }

    public function callTransportSeasonList(Request $oRequest) 
    {
        if (session('page_name') != 'ts_season' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('ts_season');

        session(['page_name' => 'ts_season']);
        
        $aData = session('ts_season') ? session('ts_season') : array();
        $oRequest->session()->forget('ts_season');

        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderBy = (Input::get('order_by')) ? Input::get('order_by') : ((count($aData)) ? $aData['order_by'] : 'desc');
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);

        session(['transport_season_listing' => array('sOrderBy'=>$sOrderBy,'sOrderField'=>$sOrderField )]);
        if (count($aData) && $sSearchStr != $aData['search_str'])
            $nPage = 1;

        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oTransportSeasonList = TransportPrice::getTransportPriceList($sSearchBy, $sSearchStr, $sOrderField, $sOrderBy, $nShowRecord);
		
		//echo "<pre>"; print_r($oTransportSeasonList);die;
		
        setSession($sSearchStr, $sSearchBy, $sOrderField, $sOrderBy, $nShowRecord, $oTransportSeasonList->currentPage(), 'ts_season');

        if ($oRequest->page > 1)
            $oViewName = 'WebView::transport._transport_season_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::transport.transport_season_list' : 'WebView::transport._transport_season_list_ajax';

        return \View::make($oViewName, compact('oTransportSeasonList', 'sSearchStr', 'sOrderField', 'sOrderBy', 'nShowRecord', 'sSearchBy'));
    }
    
    public function callTransportSeasonDelete($nid) 
    {
        TransportPrice::where('id', $nid)->delete();
        return Redirect::back();
    }

    public function callTransportPublish($nid)
    {
        $transport = Transport::find($nid);
        $is_publish=$transport->is_publish;
        if(($is_publish)=='1')
        {  
            \DB::table('ztransports')
            ->where('id', $nid)
            ->update(['is_publish' => "0"]);     
        }
        else
        {     
            \DB::table('ztransports')
            ->where('id', $nid)
            ->update(['is_publish' => "1"]);         
        }
        return 1;   

    }
    
    public function callTransportSeasonCreate(Request $oRequest,$nId = '', $nIdFrom = '') 
    {
        if ($oRequest->isMethod('post'))
    	{
            $data = Input::except('_token','price');
			unset( $data['domains']);
			
            $nSeasonId = $oRequest->id;
            $nTransportId = $oRequest->transport_id;
			
			$domains = '';
			if($oRequest->domains){
				$domains = implode(',',$oRequest->domains);
			}
            
            $aRule = [
			'name'         => 'required',
			'transport_id' => 'required_without:transport_data',
			'from'         => 'required|date',
			'to'           => 'required|date|after:date_from',
			'price'        => 'required',
			'allotment'    => 'required',
			'release'      => 'required',
			'minimum_pax'  => 'required',
			'operates'     => 'required',
			'currency_id'     => 'required',
			'transport_supplier_id' => 'required'
		];
            
            $oValidator = Validator::make($oRequest->all(), $aRule);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            
            if(isset($data['operates'])){
                $data['operates'] = implode(",",$data['operates']);
            }

            $cancellation_array = [];
            $i = 0;
            foreach ($oRequest->days as $key => $days ) {
                $percent = $data['percent'][$key];
                if($days && $percent)
                {
                    $cancellation_array[$i]['days'] = $days;
                    $cancellation_array[$i]['percent'] = $percent;
                    $i++;
                }
            }
            //$base_price = TransportBasePrice::create(['base_price' => Input::get('price')]);
            //$data['cancellation_formula'] = ($cancellation_array) ? json_encode($cancellation_array) : NULL;
            unset($data['days']);
            unset($data['percent']);
            unset($data['id']);
            unset($data['from_flag']);
            
            $base_price = TransportBasePrice::updateOrCreate(['transport_price_id'=>$nSeasonId ],['base_price' => Input::get('price')]);
			
            $markup     = TransportMarkup::where(['is_default' => 1])->first();
			
            $data       = array_merge($data, [
									'transport_base_price_id'        		=> 	$base_price->id,
									'transport_markup_id'            		=> 	$markup->id,
									'transport_markup_percentage_id' 	=> 	$markup->transport_markup_percentage_id,
									'domain_id'									=>	$domains,
									'licensee_id' 									=> 	Auth::user()->licensee_id,
									'user_id' 										=> 	Auth::user()->id,
								]);
								
			//echo "<pre>"; print_r( $data);die;			
									
            $oTransportSeason = TransportPrice::updateOrCreate(['id' => $nSeasonId],$data);
			
            $base_price = TransportBasePrice::where(['id' => $base_price->id])
                                            ->update(['transport_price_id' => $oTransportSeason->id ]);
            
            if($nSeasonId != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            if($oRequest->from_flag !=''){
                return redirect()->route('transport.transport-list');
            }
            return redirect()->route('transport.transport-season-list');
        }
        if($nIdFrom == '' && $nId != '')
        {
            $oTransportSeason = TransportPrice::where(['id'=>$nId])->with(['base_price_obj'])->first();
            //echo "<pre>";print_r($oTransportSeason);exit;
            $cancellation_formula = $oTransportSeason->cancellation_formula;
            $decoded_formula = json_decode($cancellation_formula);
        }
		
        $oTransport = Transport::with('to_city','from_city','operator','transporttype')
			->join('ztransportoperators as to', 'to.id', '=', 'ztransports.operator_id')
			->select('ztransports.*','to.name as operator_name')
			->orderBy('operator_name', 'ASC')
			->get();
			
        $oPassenger  = ['' => 'Select Passenger Type'] + PassengerType::orderBy('name','asc')->pluck('name','id')->toArray();
        $oTransportSupplier  = ['' => 'Select Supplier'] + TransportSupplier::orderBy('name','asc')->pluck('name','id')->toArray();
        $oCurrencies  = ['' => 'Select Currency'] + Currency::orderBy('code','asc')->pluck('code','id')->toArray();
        //$allDomain	=	Domain::where('status',1)->get();
        $allDomain=Domain::join('user_domains', 'user_domains.domain_id', '=', 'domains.id')
        ->where('user_domains.user_id', Auth::user()->id)  
        ->select(
           'domains.name as name',
           'user_domains.domain_id as id'
       )->orderBy('user_domains.domain_id')->get();  
        $operates   = [
			'1' => 'Monday',
			'2' => 'Tuesday',
			'3' => 'Wednesday',
			'4' => 'Thursday',
			'5' => 'Friday',
			'6' => 'Saturday',
			'7' => 'Sunday'
		];
		
        $oTransports     = [];
        foreach($oTransport as $key => $transport)
        {
            $oTransports[$transport->id] = $this->create_transport_name( $transport );
        }
		
        return \View::make('WebView::transport.transport_season_create',compact('nId','nIdFrom','decoded_formula','oTransports','oPassenger',
                                                    'oTransportSupplier','oCurrencies','operates','oTransportSeason','allDomain'));
    }

    public function callTransportOperatorList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'ts_operator' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('ts_operator');

        session(['page_name' => 'ts_operator']);
        $aData = session('ts_operator') ? session('ts_operator') : array();

        $oRequest->session()->forget('ts_operator');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : 'name');
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 't.id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oTransportList = TransportOperator::getTransportOperatorList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oTransportList->currentPage(),'ts_operator');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::transport._transport_operator_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::transport.transport_operator_list' : 'WebView::transport._transport_operator_list_ajax';
        
        return \View::make($oViewName, compact('oTransportList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));  
    }
    
    public function callTransportOperatorDelete($nId) 
    {
        $oTransport = TransportOperator::find($nId);
        $oTransport->delete();
        return 1;
    }
    
    public function callTransportOperatorCreate(Request $oRequest,$nId = '')
    {
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'name' => 'required',
                                    'marketing_contact_title' => 'required',
                                    'marketing_contact_name' => 'required',
                                    'marketing_contact_email' => 'required',
                                    'marketing_contact_phone' => 'required',
                                    'reservation_contact_name' => 'required',
                                    'reservation_contact_email' => 'required',
                                    'reservation_contact_landline' => 'required',
                                    'reservation_contact_free_phone' => 'required',
                                    'accounts_contact_name' => 'required',
                                    'accounts_contact_email' => 'required',
                                    'accounts_contact_title' => 'required',
                                    'accounts_contact_phone' => 'required',
                                    'logo' => 'required_without:id|image|dimensions:max_width=500,max_height=500|mimes:jpeg,png,jpg',
                                    'description' => 'required',
                                    'special_notes' => 'required',
                                    'remarks' => 'required',
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            
            $aData = $oRequest->except([ 'id','_token' ]);
            if(Input::hasFile('logo')){
                    $image   = Input::file('logo');
                    $image_validator = image_validator($image);
                // check if image is valid
                if( $image_validator['fails'] ){
                    //$error = ['message' => $image_validator['message']];
                }else{
                    $imageName = time().'.'.$image->getClientOriginalExtension(); 
                    $filePath = 'transport-suppliers/' . $imageName;
                    $t = \Storage::disk('s3')->put($filePath, file_get_contents($image), 'public');                   
                    $update['mapfile'] = $imageName;
                    $aData['logo'] = $filePath;
                }
            }    
            
            //insert data
            $oTransportOperator = TransportOperator::updateOrCreate(['id' => $oRequest->id],$aData);
            if($oRequest->id != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            return Redirect::route('transport.transport-operator-list');
        }
        $oTransportOperator = TransportOperator::where('id',$nId)->first();
        return \View::make('WebView::transport.transport_operator_create',compact('nId','oTransportOperator'));
        
    }
    
    public function callTransportTypeList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'ts_type' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('ts_type');

        session(['page_name' => 'ts_type']);
        $aData = session('ts_type') ? session('ts_type') : array();

        $oRequest->session()->forget('ts_type');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : 'name');
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 't.id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oTransportTypeList  = TransportType::getTransportTypeList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oTransportTypeList->currentPage(),'ts_type');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::transport._transport_type_list_ajax';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::transport.transport_type_list' : 'WebView::transport._transport_type_list_ajax';
        
        return \View::make($oViewName, compact('oTransportTypeList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));  
    }
    
    public function callTransportTypeCreate(Request $oRequest,$nId='') 
    {
        if ($oRequest->isMethod('post'))
        {
            $oValidator = Validator::make($oRequest->all(), [
                                    'name' => 'required',
                                    'transport_mode' => 'required',
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            
            $aData = $oRequest->except([ 'id','_token' ]);
            
            //insert data
            $oTransportType = TransportType::updateOrCreate(['id' => $oRequest->id],$aData);
            DB::table('cache_reload')->update(['is_reload' => 1]);
            if($oRequest->id != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            return Redirect::route('transport.transport-type-list');
        }
        $oTransportType = TransportType::where('id',$nId)->first();
        return \View::make('WebView::transport.transport_type_create',compact('nId','oTransportType'));

    }
    public function callTransportTypeSwitch($nId) 
    {
        $transport_type = TransportType::find($nId);
        $transport_type->is_enabled = Input::get('show');
        $transport_type->save();
    }
    
    public function callTransportMarkupList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'ts_markup' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('ts_markup');

        session(['page_name' => 'ts_markup']);
        $aData = session('ts_markup') ? session('ts_markup') : array();

        $oRequest->session()->forget('ts_markup');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : 'tm.name');
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'tm.id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oTransportList = TransportMarkup::getTransportMarkupList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oTransportList->currentPage(),'ts_markup');
        
        if ($oTransportList) {
            foreach ($oTransportList as $key => $value) { 
                switch ($value->allocation_type) {
                    case 'transport':
                        //print_r($value);exit;
                        $sHotel = Transport::where('id',$value['allocation_id'])->with(['from_city','to_city'])->first();
                        //echo '<pre>';print_r($sHotel->from_city->name);exit;
                        $oTransportList[$key]['allocation_name'] = 'Transport - ' .$sHotel->from_city->name.'-'.$sHotel->to_city->name;
                        break;
                    case 'city':
                        $sCity = City::where('id',$value['allocation_id'])->value('name');
                        $oTransportList[$key]['allocation_name'] = 'City - ' .$sCity;
                        break;
                    case 'country':
                        $sCountry = Country::where('id',$value['allocation_id'])->value('name');
                        $oTransportList[$key]['allocation_name'] = 'Country - ' . $sCountry;
                        break;
                    case 'all':
                        $oTransportList[$key]['allocation_name'] = 'All';
                        break;
                }
            }
        }
        //echo "<pre>";print_r($oTransportList);exit;
        $oViewName =  ($oRequest->isMethod('Get')) ? 'WebView::transport.transport_markup_list' : 'WebView::transport._more_transport_markup_list';
   
        return \View::make($oViewName, compact('oTransportList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));  
    }
    
    public function callTransportmarkupCreate(Request $oRequest, $nIdMarkup ='') 
    {
      
        if ($oRequest->isMethod('post'))
    	{
            $allocation_id_validator = '';
            if($oRequest->allocation_type != 'all')
                $allocation_id_validator = 'required';
            
            $data = Input::only('name','description','allocation_id');
            $data['allocation_type'] =  $oRequest->allocation_type;
            $data['is_active']  = ( !$oRequest->is_active ) ? FALSE: TRUE;
            $data['is_default'] = ( !$oRequest->is_default ) ? FALSE: TRUE;
            //print_r($data);exit; 
            $oValidator = Validator::make($oRequest->all(), [
                                        'name'                => 'required',
                                        'description'         => 'required',
                                        'allocation_type'     => 'required',
                                        'allocation_id'       => $allocation_id_validator,
                                        'markup_percentage'   => 'required',
                                        'agent_percentage'    => 'required',
                                        'supplier_percentage'    => 'required',
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            if( $oRequest->is_default )
                TransportMarkup::where('is_default', TRUE)->update(['is_default' => FALSE]);
            
            $oActivityMarkup = TransportMarkup::updateOrCreate(['id' => $oRequest->id_activity ], $data );
            
            $oAMP = TransportMarkupPercentage::firstOrNew(['transport_markup_id' 	=> $oActivityMarkup->id]); 
            $oAMP->percentage =  $oRequest->markup_percentage;
            $oAMP->save();
            
            $oAMAC = TransportMarkupAgentCommission::firstOrNew(['transport_markup_id' 	=> $oActivityMarkup->id]);
            $oAMAC->percentage =  $oRequest->agent_percentage;
            $oAMAC->save();
            
            $oAMS = TransportMarkupSupplierCommission::firstOrNew(['transport_markup_id' 	=> $oActivityMarkup->id]);
            $oAMS->percentage =  $oRequest->supplier_percentage;
            $oAMS->save();
            TransportMarkup::where('id', $oActivityMarkup->id)->update([
                            'transport_markup_percentage_id' => $oAMP->id,
                            'transport_markup_agent_commission_id' => $oAMAC->id,
                            'transport_markup_supplier_commission_id' => $oAMS->id
                    ]);
            if($oRequest->id_activity != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            return Redirect::to('transport/transport-markup-list');
	   
        }
        if($nIdMarkup !=''){
            $oActivityMarkup = TransportMarkup::where('id',$nIdMarkup)->with(['markup_percentage','agent_commission','supplier_commission'])->first()->toArray();
            $oActivityMarkup['markup_percentage'] = $oActivityMarkup['markup_percentage']['percentage'];
            $oActivityMarkup['agent_percentage'] = $oActivityMarkup['agent_commission']['percentage'];
            $oActivityMarkup['supplier_percentage'] = $oActivityMarkup['supplier_commission']['percentage'];
        }
                
        return \View::make('WebView::transport.transport_markup_create',compact('nIdMarkup','oActivityMarkup'));
    }
    
    public function callAllTransport()
    {
        return Transport::select('id', 'via as name')->get();
    }
    
    public function create_transport_name($transport)
    {
        $transport_name = '';
        $operator       = '';
        if($transport->operator)
        {
                if(substr(trim($transport->operator->name), -1) == 's')
                {
                        $operator = substr_replace($transport->operator->name ,"s' ",-1);
                }
                elseif( trim($transport->operator->name) == '' )
                {
                        $operator = "NO OPERATOR NAME's ";
                }
                else
                {
                        if ($transport->operator->name != NULL || trim($transport->operator->name) != '' )
                        {
                                $operator = $transport->operator->name."'s ";
                        }
                }
        }
        $transport_name = $transport_name.$operator;

        if($transport->transporttype)
        {
                $transport_name = $transport_name.$transport->transporttype->name.' ';
        }
        if($transport->from_city)
        {
                $transport_name = $transport_name.'from '.$transport->from_city->name.' ';
        }
        if($transport->to_city)
        {
                $transport_name = $transport_name.'to '.$transport->to_city->name;
        }
        return $transport_name;
    }
    


    public function callTransportAirlinesList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'airlines' || $oRequest->query('isreset') == 1)
            $oRequest->session()->forget('airlines');

        session(['page_name' => 'airlines']);
        $aData = session('airlines') ? session('airlines') : array();

        $oRequest->session()->forget('airlines');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : 'name');
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oTransportAirlineList = Airline::getTransportAirlineList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oTransportAirlineList->currentPage(),'ts_markup');
        
        $oViewName =  ($oRequest->isMethod('Get')) ? 'WebView::transport.transport_airline_list' : 'WebView::transport._more_transport_airline_list';
   
        return \View::make($oViewName, compact('oTransportAirlineList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));  
    }

    //done  by jaipur team or mayur comment by priya some changes done by me(priya) 
    public function airlines_view() 
    {
        session(['page_name' => 'airlines']);
        if (Input::has('q')) {
            $search_by = Input::has('by') ? Input::get('by') : 'airline_name';
            $airlines = Airline::where($search_by, 'like', '%' . Input::get('q') . '%')->orderBy('id', 'desc')->paginate(20);
        } else {
            $airlines = Airline::orderBy('id', 'desc')->paginate(20);
        }
        $countries = Country::all();
        return \View::make('WebView::transport.airlines',compact('airlines', 'countries'));
        //return view('transport.airlines')->with(compact('airlines', 'countries'));
    }

    public function add_airline() 
    {
        $data = Input::only('airline_code', 'airline_name', 'country_id');
        $country = Country::find($data['country_id']);
        $data['country_code'] = $country->code;
        $data['country_name'] = $country->name;
        if (Airline::firstOrCreate($data)) {
            return Redirect::back()->with('success', 'Airline Successfully Added');
        }
    }

    public function delete_airline($id) {
        $airline = Airline::find($id);
        $airline->delete();
        return 1;
    }
    

    public function callTransportAirlineCreate(Request $oRequest,$nId = '')
    {
        if ($oRequest->isMethod('post'))
        {
            $oValidator = Validator::make($oRequest->all(), [
                                    'airline_code' => 'required',
                                    'airline_name' => 'required',
                                    'country_id' => 'required',
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            $aData = $oRequest->except([ 'id','_token' ]);   
            $data = Input::only('airline_code', 'airline_name', 'country_id');
            $country = Country::find($data['country_id']);
            $data['country_code'] = $country->code;
            $data['country_name'] = $country->name;
            
            //insert data
            $oTransportAirline = Airline::updateOrCreate(['id' => $oRequest->id],$aData);
            if($oRequest->id != '')
                Session::flash('message', trans('messages.success_updated'));
            else
                Session::flash('message', trans('messages.success_inserted'));
            
            return Redirect::route('transport.airlines');
        }
        $oTransportAirline = Airline::where('id',$nId)->first();
        $countries = Country::all();
        return \View::make('WebView::transport.transport_airline_create',compact('nId','oTransportAirline', 'countries'));
        
    }

    public function update_airline($id, $action) 
    {
        $airline = Airline::find($id);
        if ($action == 'update') {
            $airline->airline_code = Input::get('airline_code');
            $airline->airline_name = Input::get('airline_name');
            $airline->country_id = Input::get('country_id');
            $country = Country::find($airline->country_id);
            $airline->country_code = $country->code;
            $airline->country_name = $country->name;
            $airline->save();
            return Redirect::back()->with('success', 'Airline Successfully Updated');
        } elseif($action == 'delete') {
            $airline->delete();
        }
    }

}