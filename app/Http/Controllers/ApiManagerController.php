<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use GuzzleHttp\Exception\RequestException;

use DB;
use Carbon\Carbon;

use App\Api;
use App\ApiSettings;

class ApiManagerController extends Controller
{

	public function index() {
		$apis = Api::orderBy('name')->get();
		return view('api-manager.index')->with(compact('apis'));
	}

	public function get() {
		return Api::with('settings')->get();
	}

	public function add_new_api() {
		$api = [
			'name' => Input::get('name'),
			'description' => Input::get('description'),
			'type' => Input::has('type') ? implode(',', Input::get('type')) : null
		];

		// check for logo
		if (Input::hasFile('logo')) {
			$file = Input::file('logo');
			$directory = public_path().'/images/api/';
			$filename = $api['name'].'.'.Input::file('logo')->getClientOriginalExtension();
			$uploaded = $file->move($directory, $filename);
			$api['logo'] = File::exists($uploaded) ? $filename : null;
		}

		// save to db
		$added = Api::firstOrCreate($api);
		if ($added) {
			return Redirect::back();
		}
	}

	public function show($api_id) {
		$api = Api::with('settings')->find($api_id);
		if ($api->settings) {
			$api->settings->json = json_decode($api->settings->json);
		}
		return view('api-manager.show')->with(compact('api'));
	}

	public function save_advanced_settings($api_id) {
		$settings = ApiSettings::firstOrNew(['api_id' => $api_id]);
		$settings->json = Input::get('settings');
		$settings->save();
		return $settings;
	}

	public function update_status($api_id) {
		$api = Api::find($api_id);
		$api->is_active = Input::get('checked');
		$api->save();
		file_get_contents(Config::get('url.eroam').'cache-apis');
	}
}
