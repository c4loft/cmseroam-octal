<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AotSupplierUpdated extends Model
{
    protected $table = 'zaotsuppliers_updated';

    protected $guarded = array('id');
}
