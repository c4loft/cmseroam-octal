<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Cmsuserlog extends Model
{
	protected $table = 'cms_user_log';
	protected $guarded = array('id');

	public function saveLog($user)
	{
		$data['user_id'] = (isset($user->id) && !empty($user->id)) ? $user->id : '';
		$data['ip_address'] = (isset($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '';
		$data['login_date'] = date('Y-m-d H:i:s');
		$data['domain_id'] = (isset($user->domain_id) && !empty($user->domain_id)) ? $user->domain_id : '';
		$this->create($data);
	}

}
