<?php 
if (!function_exists('updateTransportPricing')){
    function updateTransportPricing($data,$type,$supplier,$eroam,$licensee) {
     $e_supplier = [];
        foreach ($licensee->toArray()[0]['licensee_commission'] as $commissions) {
            if ($commissions['product_type'] == $type) {
                $l_commission = $commissions;
                break;
            }
        }
        if($supplier == 'mystifly') {
            foreach ($eroam->toArray() as $suppliers) { 
                if ($suppliers['supplier'] == $supplier) {
                    $e_supplier = $suppliers;
                    break;
                }
            }
            if(isset($data['PricedItineraries']) && isset($data['PricedItineraries']['PricedItinerary'])) {
                foreach ($data['PricedItineraries']['PricedItinerary'] as $key => $value) {
                    $price = $value['AirItineraryPricingInfo']['ItinTotalFare']['TotalFare']['Amount'];
                    if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'net') {
                        $detailPricing = net_to_net($price, $e_supplier, $l_commission);
                        $licensee_rrp = $detailPricing['licensee_rrp'];
                    }
                    if ($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'gross') {
                        $detailPricing = net_to_gross($price, $e_supplier, $l_commission);
                        $licensee_rrp = $detailPricing['reseller_rrp'];
                    }
                    if ($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'gross') {
                        $detailPricing = gross_to_gross($price, $e_supplier, $l_commission);
                        $licensee_rrp = $detailPricing['reseller_rrp'];
                    }
                    $data['PricedItineraries']['PricedItinerary'][$key]['AirItineraryPricingInfo']['ItinTotalFare']['TotalFare']['Amount'] = round($licensee_rrp, 2);
                    $data['PricedItineraries']['PricedItinerary'][$key]['AirItineraryPricingInfo']['ItinTotalFare']['TotalFare']['detailPricing'] = $detailPricing;
                }
            }
            
        } else if($supplier == 'busbud') {
            foreach ($eroam->toArray() as $suppliers) { 
                if ($suppliers['supplier'] == $supplier) {
                    $e_supplier = $suppliers;
                    break;
                }
            }

            foreach ($data as $key => $value) {
                $price = $value['price'];
                if($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'net'){                
                    $detailPricing = net_to_net($price,$e_supplier,$l_commission);
                    $licensee_rrp = $detailPricing['licensee_rrp'];
                }
                if($e_supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'gross'){                
                    $detailPricing = net_to_gross($price,$e_supplier,$l_commission);
                    $licensee_rrp = $detailPricing['reseller_rrp'];
                }
                if($e_supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'gross') {
                    $detailPricing = gross_to_gross($price,$e_supplier,$l_commission);
                    $licensee_rrp = $detailPricing['reseller_rrp'];
                }
                $data[$key]['price'] = round($licensee_rrp,2);  
                $data[$key]['detailPricing'] = $detailPricing;
            }
        } else {
            foreach ($eroam->toArray() as $suppliers) {
                if (in_array($suppliers['supplier'], $supplier)) {
                    $e_supplier[] = $suppliers;
                    break;
                }
            }

            $i = 1;
            foreach ($e_supplier as $key => $supplier) {
                if($supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'net'){
                    if($data[$key]['price_count'] <= $i) {
                        $detailPricing = net_to_net($data[$key]['price'][$i-1]['price'],$supplier,$l_commission);
                        $licensee_rrp = $detailPricing['licensee_rrp'];
                    }
                }
                if($supplier['rate_type'] == 'net' && $l_commission['rate_type'] == 'gross'){
                    if($data[$key]['price_count'] <= $i) {
                        $detailPricing = net_to_gross($data[$key]['price'][$i-1]['price'],$supplier,$l_commission);
                        $licensee_rrp = $detailPricing['reseller_rrp'];
                    }
                }
                if($supplier['rate_type'] == 'gross' && $l_commission['rate_type'] == 'gross') {
                    if($data[$key]['price_count'] <= $i) {
                        $detailPricing = gross_to_gross($data[$key]['price'][$i-1]['price'],$supplier,$l_commission);
                        $licensee_rrp = $detailPricing['reseller_rrp'];
                    }
                }
                $data[$key]['price'][$i-1]['price'] = round($licensee_rrp,2);
                $data[$key]['detailPricing'] = $detailPricing;
                $i++;
            }
        }
        return $data;
    }
}
if (!function_exists('net_to_gross'))
{   
    function net_to_gross($price,$e_supplier,$l_commission){
        $reseller_cost = floatval($price);
        $reseller_rrp_per = floatval($e_supplier['rate_percentage'])/100;
        $reseller_rrp = round(($reseller_cost/(100-(100*$reseller_rrp_per)))*100,2);
        $reseller_margin = round($reseller_rrp-$reseller_cost,2);
        $licensee_commission = floatval($l_commission['reseller_commission']);
        // $licensee_rrp = round(($reseller_rrp/(100-(100*($licensee_commission/100))))*100,2);
        $licensee_rrp = $reseller_rrp;
        $licensee_cost = $licensee_rrp-round(($licensee_rrp*($licensee_commission/100)),2);
        $licensee_benefit = $licensee_rrp-$licensee_cost;
        $reseller_benefit = $reseller_margin-$licensee_benefit;
        $licensee_id = $l_commission['licensee_id'];
        $prices = compact('licensee_id','reseller_cost','reseller_rrp_per','reseller_rrp','reseller_margin','licensee_commission','licensee_rrp','licensee_cost','licensee_benefit','reseller_benefit'); 
        return $prices;
    }
}
if (!function_exists('net_to_net'))
{   
    function net_to_net($price,$e_supplier,$l_commission){
        $reseller_cost = floatval($price);
        $reseller_rrp_per = floatval($e_supplier['rate_percentage'])/100;
        $reseller_rrp = round(($reseller_cost/(100-(100*$reseller_rrp_per)))*100,2);
        $reseller_margin = round($reseller_rrp-$reseller_cost,2);
        $licensee_commission = floatval($l_commission['reseller_commission']);
        $licensee_rrp = round(($reseller_rrp/(100-(100*($licensee_commission/100))))*100,2);
        $licensee_cost = $licensee_rrp-round(($licensee_rrp*($licensee_commission/100)),2);
        $licensee_benefit = $licensee_rrp-$licensee_cost;
        $reseller_benefit = $reseller_margin;
        $licensee_id = $l_commission['licensee_id'];
        $prices = compact('licensee_id','reseller_cost','reseller_rrp_per','reseller_rrp','reseller_margin','licensee_commission','licensee_rrp','licensee_cost','licensee_benefit','reseller_benefit'); 
        return $prices;
    }
}

if (!function_exists('gross_to_gross'))
{   
    function gross_to_gross($price,$e_supplier,$l_commission){
        $reseller_cost = $price-($price*($e_supplier['commission']/100));
        $reseller_rrp_per = floatval($e_supplier['rate_percentage'])/100;
        $reseller_rrp = round(($reseller_cost/(100-(100*$reseller_rrp_per)))*100,2);
        $reseller_margin = round($reseller_rrp-$reseller_cost,2);
        $licensee_commission = floatval($l_commission['reseller_commission'])/100;
        // $licensee_rrp = round(($reseller_rrp/(100-(100*$licensee_commission)))*100,2);
        $licensee_rrp = $reseller_rrp;
        $licensee_cost = $licensee_rrp-round(($licensee_rrp*($licensee_commission/100)),2);
        $licensee_benefit = $licensee_rrp-$licensee_cost;
        $reseller_benefit = $reseller_margin-$licensee_benefit;
        $licensee_id = $l_commission['licensee_id'];
        $prices = compact('licensee_id','reseller_cost','reseller_rrp_per','reseller_rrp','reseller_margin','licensee_commission','licensee_rrp','licensee_cost','licensee_benefit','reseller_benefit'); 
        return $prices;
    }
}

if (!function_exists('gross_to_net'))
{   
    function gross_to_net($price,$e_supplier,$l_commission){
        $reseller_cost = $price-($price*($e_supplier['commission']/100));
        $reseller_rrp_per = floatval($e_supplier['rate_percentage'])/100;
        $reseller_rrp = round(($reseller_cost/(100-(100*$reseller_rrp_per)))*100,2);
        $reseller_margin = round($reseller_rrp-$reseller_cost,2);
        $licensee_commission = floatval($l_commission['reseller_commission'])/100;
        $licensee_rrp = round(($reseller_rrp/(100-(100*$licensee_commission)))*100,2);
        $licensee_cost = $licensee_rrp-round(($licensee_rrp*($licensee_commission/100)),2);
        $licensee_benefit = $licensee_rrp-$licensee_cost;
        $reseller_benefit = $reseller_margin;
        $licensee_id = $l_commission['licensee_id'];
        $prices = compact('licensee_id','reseller_cost','reseller_rrp_per','reseller_rrp','reseller_margin','licensee_commission','licensee_rrp','licensee_cost','licensee_benefit','reseller_benefit'); 
        return $prices;
    }
}
