<?php
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;
return [
    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */
    'default' => env('LOG_CHANNEL', 'daily'),
    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */
    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['stack'],
        ],
        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
        ],
        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => 7,
        ],
        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => 'critical',
        ],
        'papertrail' => [
            'driver'  => 'monolog',
            'level' => 'debug',
            'handler' => SyslogUdpHandler::class,
            'handler_with' => [
                'host' => env('PAPERTRAIL_URL'),
                'port' => env('PAPERTRAIL_PORT'),
            ],
        ],
        'stderr' => [
            'driver' => 'monolog',
            'handler' => StreamHandler::class,
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],
        'syslog' => [
            'driver' => 'syslog',
            'level' => 'debug',
        ],
        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
        ],
        'expedia_hotels' => [
            'driver' => 'daily',
            'path' => storage_path('logs/expedia/listHotel/expedia.log'),
            'days' => 7,
        ],
        'expedia_avaibility' => [
            'driver' => 'daily',
            'path' => storage_path('logs/expedia/hotelAvaibility/expedia.log'),
            'days' => 7,
        ],
        'expedia_info' => [
            'driver' => 'daily',
            'path' => storage_path('logs/expedia/hotelInfo/expedia.log'),
            'days' => 7,
        ],
        'expedia_booking' => [
            'driver' => 'daily',
            'path' => storage_path('logs/expedia/hotelBooking/expedia.log'),
            'days' => 7,
        ],
        'expedia_voucher' => [
            'driver' => 'daily',
            'path' => storage_path('logs/expedia/hotelVoucher/expedia.log'),
            'days' => 7,
        ],
        'viator_products' => [
            'driver' => 'daily',
            'path' => storage_path('logs/viator/searchProduct/viator.log'),
            'days' => 7,
        ],
        'viator_booking' => [
            'driver' => 'daily',
            'path' => storage_path('logs/viator/booking/viator.log'),
            'days' => 7,
        ],
        'viator_detail' => [
            'driver' => 'daily',
            'path' => storage_path('logs/viator/searchProductDetail/viator.log'),
            'days' => 7,
        ],
        'viator_hotels' => [
            'driver' => 'daily',
            'path' => storage_path('logs/viator/hotels/viator.log'),
            'days' => 7,
        ],
        'viator_tourgrade' => [
            'driver' => 'daily',
            'path' => storage_path('logs/viator/tourGrade/viator.log'),
            'days' => 7,
        ],
        'mystifly_search' => [
            'driver' => 'daily',
            'path' => storage_path('logs/mystifly/AirLowFareSearch/mystifly.log'),
            'days' => 7,
        ],
        'mystifly_rules' => [
            'driver' => 'daily',
            'path' => storage_path('logs/mystifly/FareRules/mystifly.log'),
            'days' => 7,
        ],
        'mystifly_revalidate' => [
            'driver' => 'daily',
            'path' => storage_path('logs/mystifly/AirRevalidate/mystifly.log'),
            'days' => 7,
        ],
        'mystifly_book' => [
            'driver' => 'daily',
            'path' => storage_path('logs/mystifly/Booking/mystifly.log'),
            'days' => 7,
        ],
        'mystifly_ticket' => [
            'driver' => 'daily',
            'path' => storage_path('logs/mystifly/TicketOrder/mystifly.log'),
            'days' => 7,
        ],
        'mystifly_notes' => [
            'driver' => 'daily',
            'path' => storage_path('logs/mystifly/BookingNotes/mystifly.log'),
            'days' => 7,
        ],
        'mystifly_details' => [
            'driver' => 'daily',
            'path' => storage_path('logs/mystifly/TripDetails/mystifly.log'),
            'days' => 7,
        ],
        'roomerflex_request' => [
            'driver' => 'daily',
            'path' => storage_path('logs/roomerflex/RequestToken/roomerflex.log'),
            'days' => 7,
        ],
        'roomerflex_protection' => [
            'driver' => 'daily',
            'path' => storage_path('logs/roomerflex_protection/RoomerflexProtection/roomerflex.log'),
            'days' => 7,
        ],
        'busbud' => [
            'driver' => 'daily',
            'path' => storage_path('logs/busbud/busbud.log'),
            'days' => 7,
        ],
        'yalago_hotels' => [
            'driver' => 'daily',
            'path' => storage_path('logs/yalago/listHotel/yalago.log'),
            'days' => 7,
        ],
        'yalago_static' => [
            'driver' => 'daily',
            'path' => storage_path('logs/yalago/staticData/yalago.log'),
            'days' => 7,
        ],
        'yalago_avaibility' => [
            'driver' => 'daily',
            'path' => storage_path('logs/yalago/hotelAvaibility/yalago.log'),
            'days' => 7,
        ],
        'yalago_info' => [
            'driver' => 'daily',
            'path' => storage_path('logs/yalago/hotelInfo/yalago.log'),
            'days' => 7,
        ],
        'yalago_booking' => [
            'driver' => 'daily',
            'path' => storage_path('logs/yalago/hotelBooking/yalago.log'),
            'days' => 7,
        ],
        'yalago_voucher' => [
            'driver' => 'daily',
            'path' => storage_path('logs/yalago/hotelVoucher/yalago.log'),
            'days' => 7,
        ],
    ],
];
