<?php

return [
	
	'USERTYPEADMIN' => 'admin',
	'USERTYPEAGENT' =>'agent',
	'USERTYPELICENSEE' =>'licensee',
	'USERTYPECUSTOMER' =>'customer',
	'USERTYPEPRODUCT' =>'product',
	'USERTYPEBRAND' =>'brand',    
    //pagination
	'PERPAGERECORDS' =>10,
];