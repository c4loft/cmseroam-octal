<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZcitiesLatlangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zcitieslatlang'))
        {
        Schema::create('zcitieslatlang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->unsigned();
            $table->string('lat',50);
            $table->string('lng',50);
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('city_id')->references('id')->on('zcities');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zcitieslatlang');
    }
}
