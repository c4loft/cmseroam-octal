<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZaecitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zaecities'))
        {
        Schema::create('zaecities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('RegionId')->nullable();
            $table->integer('CountryId')->nullable();
            $table->string('Code',255)->nullable();
            $table->string('name',255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zaecities');
    }
}
