<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZhotelpriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zhotelprices')){ 
        Schema::create('zhotelprices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hotel_id');
            $table->integer('hotel_room_type_id');
            $table->tinyInteger('with_breakfast')->default(0);
            $table->integer('allotment');
            $table->integer('release');
            $table->integer('minimum_pax');
            $table->integer('hotel_season_id')->nullable();
            $table->integer('hotel_base_price_id')->nullable();
            $table->integer('hotel_markup_percentage_id')->nullable();
            $table->integer('hotel_markup_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhotelprices');
    }
}
