<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZtransportpriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('ztransportprice'))
        {
        Schema::create('ztransportprice', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transport_id');
            $table->string('name')->nullable();
            $table->string('minimum_pax')->nullable();
            $table->string('release')->nullable();
            $table->string('allotment')->nullable();
            $table->timestamps();
            $table->string('operates')->nullable();
            $table->integer('passenger_type_id')->nullable();
            $table->date('[from]')->nullable();
            $table->date('[to]')->nullable();
            $table->bigInteger('transport_base_price_id')->nullable();
            $table->bigInteger('transport_markup_percentage_id')->nullable();
            $table->bigInteger('transport_markup_id')->nullable();
            $table->softDeletes();
            $table->text('cancellation_policy')->nullable();
            $table->text('cancellation_formula')->nullable();
            $table->integer('currency_id')->nullable();
            $table->bigInteger('transport_supplier_id')->nullable();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ztransportprice');
    }
}
