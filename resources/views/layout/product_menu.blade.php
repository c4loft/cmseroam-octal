<div class="page-sidebar-wrapper">
        <nav class="pushy pushy-left" style="visibility: visible;">
            <div class="pushy-height">
                    {{ Auth::user()->type }}
                    <ul>
                    <li class="expand-all"><a href="javascript://"><i class="icon-unfold-more"></i><span>Expand All Menu Items</span></a></li>
                    <li class="collapse-all active"><a href="javascript://"><i class="icon-unfold-less"></i><span>Collapse All Menu Items</span></a></li>
                    <div class="divider-text">Accounts &amp; Reporting</div>
                    <li class="pushy-submenu {{ (session('page_name')=='dashboard') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}"><a href="{{url('/')}}"><i class="icon-dashboard"></i><span>Dashboard</span></a></li>
    
                    <li class="pushy-submenu {{ (session('page_name')=='itenary' || session('page_name')=='book_tour') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                        <a href="javascript://"><i class="icon-booking"></i><span>{{ trans('messages.booking_management') }}</span></a>
                        <ul>
                            <li class="pushy_link_cls {{ (session('page_name') == 'itenary' ? 'active' : '') }}"><a href="{{ route('booking.itenary-list') }}">{{ trans('messages.itinerary_bookings') }}</a></li>
                            <li class="pushy_link_cls {{ (session('page_name') == 'book_tour' ? 'active' : '') }}"><a href="{{ route('booking.booked-tour-list') }}">{{ trans('messages.tour_bookings') }}</a></li>
                            {{-- <li class="pushy_link_cls {{ (session('page_name') == 'viator_activity_booking' ? 'active' : '') }}"><a href="{{ route('booking.viator-booking-list') }}">Viator Bookings</a></li> --}}
                        </ul>
                    </li>
                    <li class="{{ (session('page_name') && session('page_name') == 'currency' ? 'active' : '') }}">
                        <a href="{{ route('common.currency-list') }}"><i class="icon-currency"></i><span>{{ trans('messages.currency_management') }}</span></a>
                    </li>
    
    
                    <li class="{{ (session('page_name') && session('page_name') == 'label' ? 'active' : '') }}">
                        <a href="{{ route('common.label-list').'?isreset=1' }}">
                            <i class="icon-label"></i>
                            <span>{{ trans('messages.label') }}</span>
                        </a>
                    </li>
    
                    <li class="pushy-submenu {{ (session('page_name') == 'region' || session('page_name') == 'city' || session('page_name') == 'country' || session('page_name') == 'suburb' ) ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}"><a href="javascript://"><i class="icon-geodata"></i><span>{{ trans('messages.geo_data') }}</span></a>
                        <ul>
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'region' ? 'active' : '') }}"><a href="{{ route('common.region-list').'?isreset=1' }}">{{ trans('messages.manage_region') }}</a></li>
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'country' ? 'active' : '') }}"><a href="{{ route('common.country-list').'?isreset=1' }}">{{ trans('messages.manage_country') }}</a></li>
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'city' ? 'active' : '') }}"><a href="{{ route('common.city-list').'?isreset=1' }}">{{ trans('messages.manage_city') }}</a></li>
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'suburb' ? 'active' : '') }}"><a href="{{ route('common.suburb-list').'?isreset=1' }}">{{ trans('messages.manage_suburb') }}</a></li>
                        </ul>
                    </li>
    
                    <div class="divider-text">{{ trans('messages.client_onboarding') }}</div>
    
                     <li class="pushy-submenu {{ (session('page_name') == 'onboarding' || session('page_name') == 'onboarding_list') ? 'pushy-submenu-open active open_pushy_cls':'pushy-submenu-closed' }}"><a href="javascript://"><i class="icon-user"></i><span>{{ trans('messages.client_onboarding') }}</span></a>
                        <ul>
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'onboarding' ? 'active' : '') }}"><a href="{{ route('onboarding.general') }}">{{ trans('messages.client_onboarding') }}</a></li>
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'onboarding_list' ? 'active' : '') }}"><a href="{{ route('onboarding') }}">{{ trans('messages.list') }}</a></li>
                            
                        </ul>
                    </li>
    
                    
                    <div class="divider-text">{{ trans('messages.product_management') }}</div>
    
                    <li class="pushy-submenu {{ (session('page_name')=='acomodation' || session('page_name')=='ac_supplier'|| session('page_name')=='ac_label'
                        || session('page_name')=='ac_season' || session('page_name')=='ac_price' || session('page_name')=='ac_markup' || session('page_name')=='ac_room' 
                        || session('page_name')=='aot_location'|| session('page_name')=='aot_supplier' || session('page_name')=='expedia_hotel_mapping' ) ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                        <a href="javascript://"><i class="icon-hotel"></i>
                            <span>{{ trans('messages.acomodation_management') }}</span>
                        </a>
                        <ul> 
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'acomodation' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-list').'?isreset=1' }}">{{ trans('messages.acomodation_management') }}</a></li>
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'ac_supplier' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-supplier-list').'?isreset=1' }}">{{ trans('messages.manage_suppliers') }}</a></li>
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'ac_label' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-label-list').'?isreset=1' }}">{{ trans('messages.manage_mapping') }}</a></li>
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'ac_season' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-season-list').'?isreset=1' }}">{{ trans('messages.manage_seasons') }}</a></li>
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'ac_price' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-price-list').'?isreset=1' }}">{{ trans('messages.manage_prices') }}</a></li>
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'ac_markup' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-markup-list').'?isreset=1' }}">{{ trans('messages.manage_markup') }}</a></li>                  
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'ac_room' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-room-list').'?isreset=1' }}">{{ trans('messages.manage_roomtype') }}</a></li>                  
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'aot_location' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-aotlocation-list') }}">{{ trans('messages.manage_aotlocation') }}</a></li>                  
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'aot_supplier' ? 'active' : '') }}"><a href="{{ route('acomodation.hotel-aotsupplier-list').'?isreset=1' }}">{{ trans('messages.manage_aotsupplier') }}</a></li>    
                            <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'expedia_hotel_mapping' ? 'active' : '') }}"><a href="{{ route('acomodation.expedia-hotel-mapping').'?isreset=1' }}">{{ trans('messages.manage_expedia_hotel') }}</a></li>              
                        </ul>
                    </li>
                    <li class="pushy-submenu {{ in_array(session("page_name") ,array('transport','ts_supplier','ts_operator','ts_season','ts_type','ts_markup','airlines')) ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                        <a href="javascript://"><i class="icon-transport"></i><span>{{ trans('Transport Management') }}</span></a>
                        <ul> 
                            <li class="pushy_link_cls {{ (session('page_name') == 'transport' ? 'active' : '') }}"><a href="{{ route('transport.transport-list').'?isreset=1' }}">{{ trans('Manage Transport') }} </a></li>
                            <li class="pushy_link_cls {{(session('page_name') == 'ts_supplier' ? 'active' : '') }}"><a href="{{ route('transport.transport-supplier-list').'?isreset=1' }}">Manage Suppliers</a></li>
                            <li class="pushy_link_cls {{(session('page_name') == 'ts_operator' ? 'active' : '') }}"><a href="{{ route('transport.transport-operator-list').'?isreset=1' }}">{{ trans('Manage Operator') }}</a></li>
                            <li class="pushy_link_cls {{(session('page_name') == 'ts_season' ? 'active' : '') }}"><a href="{{ route('transport.transport-season-list').'?isreset=1' }}">{{ trans('Manage Season') }}</a></li>
                            <li class="pushy_link_cls {{(session('page_name') == 'ts_type' ? 'active' : '') }}"><a href="{{ route('transport.transport-type-list').'?isreset=1' }}">{{ trans('Manage Transport Type') }}</a></li>
                            <li class="pushy_link_cls {{(session('page_name') == 'ts_markup' ? 'active' : '') }}"><a href="{{ route('transport.transport-markup-list').'?isreset=1' }}">{{ trans('Manage Mark-Up') }}</a></li>
                            <li class="pushy_link_cls {{(session('page_name') == 'airlines' ? 'active' : '') }}"><a href="{{ route('transport.airlines').'?isreset=1' }}">{{ trans('Manage Airlines') }}</a></li>
                        </ul>
                    </li>
                    <li class="pushy-submenu {{ (session('page_name')=='activity' || session('page_name')=='activity_supplier' || session('page_name')=='activity_season' 
                                                || session('page_name')=='activity_mark'|| session('page_name')=='activity_operator') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                        <a href="javascript://"><i class="icon-activity"></i><span>{{ trans('messages.activity_management') }}</span></a>
                        <ul>  
                            <li class="pushy_link_cls {{ (session('page_name') == 'activity' ? 'active' : '') }}">
                                <a href="{{ route('activity.activity-list') }}">{{ trans('messages.manage_activity') }}</a>
                            </li>
                            <li class="pushy_link_cls {{ (session('page_name') == 'activity_supplier' ? 'active' : '') }}"><a href="{{ route('activity.activity-supplier-list').'?isreset=1' }}">{{ trans('messages.manage_suppliers') }}</a></li>
                            <li class="pushy_link_cls {{ (session('page_name') == 'activity_season' ? 'active' : '') }}"><a href="{{ route('activity.activity-season-list').'?isreset=1' }}">{{ trans('messages.manage_seasons') }}</a></li>
                            <li class="pushy_link_cls {{ (session('page_name') == 'activity_mark' ? 'active' : '') }}"><a href="{{ route('activity.activity-markup-list').'?isreset=1' }}">{{ trans('messages.manage_markup') }}</a></li>
                            <li class="pushy_link_cls {{ (session('page_name') == 'activity_operator' ? 'active' : '') }}"><a href="{{ route('activity.activity-operator-list').'?isreset=1' }}">{{ trans('messages.manage_operator') }}</a></li>
                        </ul>
                    </li>
    
                    <li class="{{ (session('page_name') && session('page_name') == 'route' ? 'active' : '') }}">
                        <a href="{{ route('route.route-list').'?isreset=1' }}"><i class="icon-route"></i><span>{{ trans('messages.route_plans') }}</span></a>
                    </li>
    
                    <div class="divider-text">{{ trans('messages.tour_management') }}</div>
                    <li class="pushy-submenu {{ (session('page_name')=='tourlist' || session('page_name')=='addtour' || session('page_name')=='tourtypelogo') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                        <a href="javascript://"><i class="icon-activity"></i><span>{{ trans('messages.tour_management') }}</span></a>
                        <ul> 
                            <li class="pushy_link_cls {{ (session('page_name') == 'tourlist' ? 'active' : '') }}"><a href="{{ route('tour.tour-list').'?isreset=1' }}">{{ trans('messages.search_tour') }} </a></li>
                            <li class="pushy_link_cls {{(session('page_name') == 'addtour' ? 'active' : '') }}"><a href="{{ route('tour.tour-create').'?isreset=1' }}">{{ trans('messages.add_tour') }} </a></li>
                            <li class="pushy_link_cls {{(session('page_name') == 'tourtypelogo' ? 'active' : '') }}"><a href="{{ route('tour.tour-type-logo-list').'?isreset=1' }}">{{ trans('messages.tour_type_logo') }}</a></li>
                        </ul>
                    </li>
                    <div class="divider-text">{{ trans('messages.market_place') }}</div>
                    <li class="{{ (session('page_name') && session('page_name') == 'specialoffer' ? 'active' : '') }}">
                        <a href="{{ route('special-offer.list') }}">
                            <i class="icon-currency"></i>
                            <span>{{ trans('messages.special_offer') }}</span>
                        </a>
                    </li>
    
                </ul>
            </div>
        </nav>
    </div>