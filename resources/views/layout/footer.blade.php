
<script type="text/javascript" src="{{ asset('assets/jquery-ui/jquery-ui.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/pushy.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/main.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/additional-methods.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datepicker.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/clockface.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/responsive.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/prettify.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.slimscroll.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/developer.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.fileuploader.min.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/tinymce/js/tinymce/tinymce.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/highlight.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-switch.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/main1.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/js/selectize.js') }}" ></script>

  
    @yield( 'custom-js' )

    @stack('scripts')
 </body>
</html>